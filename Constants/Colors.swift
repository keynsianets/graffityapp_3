//
//  Color.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/10/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit


struct Colors {
    static let grayTextColor = UIColor(rgb: 0x9DABC0)
    static let darkSkyBlue = UIColor(rgb: 0x3B7CEC)
    static let placeholderActiveColor = UIColor(rgb: 0x505050)
    static let placeholderColor = UIColor(rgb: 0xe6e6e6)
    static let brownishGrey = UIColor(rgb: 0x5f5f5f)
    static let scarlet = UIColor(red: 208.0 / 255.0, green: 2.0 / 255.0, blue: 27.0 / 255.0, alpha: 1.0)
static let lightGreyBlue = UIColor(rgb: 0x9dabc0)
 
    static let white = UIColor.white
    static let clear = UIColor.clear
    
    
    static let background = UIColor(named: "backgroundColor", defaultColorRgb: 0xffffff)
    static let text = UIColor(named: "textColor", defaultColorRgb:  0x5f5f5f)
    static let tabBar = UIColor(named: "tabBarColor", defaultColorRgb:  0xffffff)
    static let tabBarText = UIColor(named: "tabBarText", defaultColorRgb:  0x9dabc0)
    static let backButton = UIColor(named: "backButtonColor", defaultColorRgb:  0x9dabc0)
    
    static let backgroundName = "backgroundColor"
    static let textName = "textColor"
    static let tabBarName = "tabBarColor"
    static let tabBarTextName = "tabBarText"
    static let backButtonName = "backButtonColor"
    static let cellBackgroundName = "cellBackground"
    static let backgroundSegmentName = "backgroundSegment"
    static let blueColorName = "blueColorApp"
    static let tabBarBackgroundName = "tabBarColor"
}


extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    convenience init(named: String, defaultColorRgb: Int) {
        
        if #available(iOSApplicationExtension 11.0, *) {
            guard let color = UIColor(named: named) else {
                self.init(
                    red: (defaultColorRgb >> 16) & 0xFF,
                    green: (defaultColorRgb >> 8) & 0xFF,
                    blue: defaultColorRgb & 0xFF
                )
                return
            }
            self.init(cgColor: color.cgColor)
        } else {
            self.init(
                red: (defaultColorRgb >> 16) & 0xFF,
                green: (defaultColorRgb >> 8) & 0xFF,
                blue: defaultColorRgb & 0xFF
            )
        }
        
        
    }
}
