//
//  Fonts.swift
//  GraffityApp
//
//  Created by Alex Suvorov on 9/4/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import Foundation


struct Fonts {
    static let avenirLight = "Avenir-Light"
    static let avenirBlack = "Avenir-Black"
    static let avenirHeavy = "Avenir-Heavy"
    static let avenirMedium = "Avenir-Medium"
    static let avenirBook = "Avenir-Book"
    static let avenirRoman = "Avenir-Roman"
    static let avenirLightOblique = "Avenir-LightOblique"
    static let robotoLight = "Roboto-Light"
}
