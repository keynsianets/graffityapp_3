//
//  Strings.swift
//  PhotoExif
//
//  Created by Денис Марков on 3/29/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import Foundation

struct Strings {
    //General photo data
    static let colorModel = "Color Model"
    static let dpiHeight = "DPI Height"
    static let dpiWidth = "DPI Width"
    static let depth = "Depth"
    static let orientation = "Orientation"
    static let profileName = "Profile Name"
    static let noDataGeneral = "Data not available"
    static let somethingWentWrong = "Something went wrong"
    
    
    //MARK: StartViewController
    static let appName = "GraffitiTagr"
    static let signUp = "Sign Up"
    static let login = "Login"
    static let slogan = "Keep our city clean"
    
    //MARK: FascebookSignupViewController
    static let newAccount = "New account"
    static let completeRegistration = "Please complete your registration"
    static let userName = "Username"
    static let email = "Email"
    static let continueLabel = "Continue"
    static let iAlreadyHaveAccount = "I already have an account. Login"
    static let signUpFailed = "Sign up failed! "
    static let usernameCannotBeEmpty = "Username field cannot be blank"
    static let emailCannotBeEmpty = "Email field cannot be blank"
    static let error = "Error"
    
    //MARK: WelcomeViewController
    static let prev = "Prev"
    static let next = "Next"
    static let protectYourCity = "Protect Your City"
    static let helpMakeBeautiful = "Help make the environment beautiful"
    static let shareYourSnap = "Share Your Snap"
    static let takeAPhotoForPlaceToBeautiful = "Take a photo of where you want to be beautified"
    static let cleanYourCity = "Clean Your City"
    static let helpToLiveInClean = "Help you to live in a clean city"
    
    //MARK: LocationRequestViewController
    static let accessYour = "Access Your"
    static let location = "Location"
    static let pleaseEnableLocation = "Please enable location services to share your location"
    static let doNotAllow = "Do not allow"
    static let allowLocation = "Allow locations"
    
    //MARK: AddFirstSnapeViewController
    static let addFirstSnap = "Add First Snap"
    static let pleaseShareYourFirstSnap = "Please share your first snap"
    static let takePhoto = "Take a photo"
    
    //MARK: CameraViewController
    static let ok = "Ok"
    static let cantGetGPSData = "Cant't get GPS data"
    static let checkYouAllowCameraGetLocation = "Check you allow camera to get your location and make a new photo"
    static let cantGetEXIF = "Can't get photo EXIF data"
    
    //MARK: UploadPhotoViewController
    static let addNew = "Add New"
    static let title = "Title"
    static let typeHere = "Type here..."
    static let back = "Back"
    static let save = "Save"
    
    //MARK: EditGraffityViewController
    static let delete = "Delete"

    //MARK: ListViewController
    static let yourPostHasBeenDeleted = "Your post has been deleted"
    
    //MARK: MapViewController
    static let haveNoAccessToLocation = "Have no access to location"
    static let youHaveToAllowApplicationToUseLocation = "You have to allow application to use location for use map functions"
    
    //MARK: FullImageViewController
    static let backWithArrow = "< back"
    
    //MARK: EditProfileViewController
    static let editUser = "Edit User"
    static let password = "Password"
    static let update = "Update"
    
    //MARK: UserProfileViewController
    static let editUserInformation = "Edit User Information"
    static let notifications = "Notifications"
    static let languageSettings = "Language Settings"
    static let logout = "Logout"
    
    //MARK: SignUpViewController
    static let fullName = "Full Name"
    static let register = "Register"
    static let signUpWithFacebook = "Sign up with Facebook"
    static let fieldCannotBeBlank = " field cannot be blank"
    static let ensurePasswordHasBeenLeast8Characters = "Ensure password has at least 8 characters."
    static let somethingWrongTryAgain = "Something went wrong. Try again later."
    
    //MARK: LoginViewController
    static let hello = "Hello"
    static let signInToAccessYourAccount = "Sign in to access your account."
    static let emailAddress = "Email address"
    static let signIn = "Sign in"
    static let signInWithFacebook = "Sign in with Facebook"
    static let registerHere = "Register here"
    static let forgotPassword = "Forgot password"
    static let dontHaveAccount = "Don't have an account?"
    static let signInFailed = "Sign in failed"
    static let usernameAndPasswordDidntMatch = "Username and password didn't match."
    static let wrongEmailOrPassword = "Wrong email/password"
    
    //MARK: GraffityActionsTableViewCell
    static let solved = "Solved"
    static let edit = "Edit"

    static let map = "Map"
    static let list = "List"
}
