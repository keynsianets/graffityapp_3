//
//  ExtensionAlertViewController.swift
//  GraffitiTagrShareExtension
//
//  Created by Denis Markov on 9/18/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit

class ExtensionAlertViewController: UIViewController {
    
    @IBOutlet weak var testLabel: UILabel! {
        didSet {
            testLabel.text = "Can't get GPS info"
        }
    }
    
    @IBOutlet weak var cancelButton: UIButton! {
        didSet {
            cancelButton.setTitle("Cancel", for: .normal)
            cancelButton.addTarget(self, action: #selector(cancelButtonDidTap), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var postButton: UIButton! {
        didSet {
            postButton.setTitle("Post", for: .normal)
            postButton.addTarget(self, action: #selector(postButtonDidTap), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var imageView: UIImageView!
    
    var exif: [String: Any]?
    var image: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.testLabel.isHidden = true
        getImageURL()
    }
    
    func getImageURL() {
        let typeIdentifier = "public.image"
        if let item = extensionContext?.inputItems.first as? NSExtensionItem, let itemProvider = item.attachments?.first {
            if itemProvider.hasItemConformingToTypeIdentifier(typeIdentifier) {
                itemProvider.loadItem(forTypeIdentifier: typeIdentifier, options: nil) { (urlItem, error) in
                    if let url = urlItem as? URL {
                        if let imageSource = CGImageSourceCreateWithURL(url as CFURL, nil) {
                            let imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil)
                            DispatchQueue.main.async {
                                 self.testLabel.isHidden = false
                            }
                            if let dict = imageProperties as? [String: Any] {
                                if let orientation = dict["Orientation"] as? Int, let image = CGImageSourceCreateImageAtIndex(imageSource, 0, nil) {
                                    var imageOrientation: UIImage.Orientation?
                                    switch orientation {
                                    case 1:
                                        imageOrientation = .up
                                    case 2:
                                        imageOrientation = .up
                                    case 3:
                                        imageOrientation = .down
                                    case 4:
                                        imageOrientation = .down
                                    case 5:
                                        imageOrientation = .left
                                    case 6:
                                        imageOrientation = .right
                                    case 7:
                                        imageOrientation = .right
                                    case 8:
                                        imageOrientation = .left
                                    default:
                                        imageOrientation = nil
                                    }
                                    if let imgOrientaition = imageOrientation {
                                        DispatchQueue.main.async {
                                            let image = UIImage(cgImage: image, scale: 1, orientation: imgOrientaition)
                                            self.imageView.image = image
                                            self.image = image
                                        }
                                    } else {
                                        let image = UIImage(cgImage: image)
                                        self.imageView.image = image
                                        self.image = image
                                    }
                                } else if let image = CGImageSourceCreateImageAtIndex(imageSource, 0, nil){
                                    let image = UIImage(cgImage: image)
                                    self.imageView.image = image
                                    self.image = image
                                }
                                self.exif = dict
                                if CheckGPSisPresent.check(dict: dict) {
                                    DispatchQueue.main.async {
                                        self.testLabel.isHidden = true
                                    }
                                }
                            } else if let image = CGImageSourceCreateImageAtIndex(imageSource, 0, nil){
                                let image = UIImage(cgImage: image)
                                self.imageView.image = image
                                self.image = image
                            }
                        }
                    }
                }
            }
        } else {
            self.testLabel.isHidden = false
            self.testLabel.text = "Can't load image"
        }
    }
    
    @objc func cancelButtonDidTap() {
        self.extensionContext?.completeRequest(returningItems: [], completionHandler: nil)
    }
    
    @objc func postButtonDidTap() {
        guard let image = image else {
            self.testLabel.isHidden = false
            self.testLabel.text = "Can't load image"
            return
        }
        let title = "Empty title"
        let source = "CAMERA_ROLL"
        var locInstance: Location?
        if let location = LocationManager.shared.locationManager.location?.coordinate {
            locInstance = Location(coordinates: location)
        }
        let imageToServer = ImageToServer(title: title, location: locInstance, source: source)
        DispatchQueue.main.async {
            self.view.showActivityIndicator()
        }
        RestAPI().createNewGraffity(data: imageToServer, image: image, metadata: self.exif) { (isOk) in
            DispatchQueue.main.async {
                self.view.hideActivityIndicator()
                if isOk {
                    self.extensionContext!.completeRequest(returningItems: [], completionHandler: nil)
                } else {
                    self.testLabel.isHidden = false
                    self.testLabel.text = "Can't upload image"
                }
            }
        }
    }

}

extension UIView {
    func showActivityIndicator() {
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        activityIndicator.backgroundColor = .clear
        activityIndicator.center = self.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .whiteLarge
        activityIndicator.color = .blue
        activityIndicator.startAnimating()
        
        activityIndicator.tag = 100
        for subview in self.subviews {
            if subview.tag == 100 {
                print("already added")
                return
            }
        }
        
        self.addSubview(activityIndicator)
    }
    
    func hideActivityIndicator() {
        let activityIndicator = self.viewWithTag(100) as? UIActivityIndicatorView
        activityIndicator?.stopAnimating()
        activityIndicator?.removeFromSuperview()
    }
}

extension UIImage {
    
    func fixedOrientation() -> UIImage? {
        
        guard imageOrientation != UIImage.Orientation.up else {
            //This is default orientation, don't need to do anything
            return self.copy() as? UIImage
        }
        
        guard let cgImage = self.cgImage else {
            //CGImage is not available
            return nil
        }
        
        guard let colorSpace = cgImage.colorSpace, let ctx = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) else {
            return nil //Not able to create CGContext
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat.pi)
            break
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat.pi / 2.0)
            break
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat.pi / -2.0)
            break
        case .up, .upMirrored:
            break
        @unknown default:
            break
        }
        
        //Flip image one more time if needed to, this is to prevent flipped image
        switch imageOrientation {
        case .upMirrored, .downMirrored:
            transform.translatedBy(x: size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case .leftMirrored, .rightMirrored:
            transform.translatedBy(x: size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case .up, .down, .left, .right:
            break
        @unknown default:
            break
        }
        
        ctx.concatenate(transform)
        
        switch imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        default:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }
        
        guard let newCGImage = ctx.makeImage() else { return nil }
        return UIImage.init(cgImage: newCGImage, scale: 1, orientation: .up)
    }
}
