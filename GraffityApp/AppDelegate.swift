//
//  AppDelegate.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/10/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKCoreKit
import OneSignal
import GoogleMaps
import Bugsnag

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var mainTabBarVC : MainTabBarViewController?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Bugsnag.start(withApiKey: "c28314f67a63fa4872292ec06769300e")
        GMSServices.provideAPIKey("AIzaSyDEfKHkBu-5_bznEVWs83J4K4yLBVd6BCo")
//        Bugsnag.notifyError(NSError(domain:"com.example", code:408, userInfo:nil))
        IQKeyboardManager.shared.enable = true
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        //UIApplication.shared.delegate?.application?(application, didFinishLaunchingWithOptions: launchOptions)
      //  Settings.setAutoLogAppEventsEnabled(true)
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        
        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "989cae32-2c05-4603-b1ce-273d8a92f197",
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        let firstViewController  = storyboard.instantiateViewController(withIdentifier: "StartVC")
        
       
        
        
        let aObjNavi = UINavigationController(rootViewController: firstViewController)
        aObjNavi.isNavigationBarHidden = true
        window = UIWindow(frame: UIScreen.main.bounds)
        window!.rootViewController = aObjNavi
        if let _ = UserProfile.shared.token {
            aObjNavi.pushViewController(MainTabBarViewController(), animated: false)
        }
        window!.makeKeyAndVisible()
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        return ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }

    
//    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//        let handled = ApplicationDelegate.shared.application(app, open: url, options: options)
//        
//        return handled
//    }
//    
//    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
//        if let rootViewController = self.topViewControllerWithRootViewController(rootViewController: window?.rootViewController) {
//            if (rootViewController.responds(to: Selector(("canRotate")))) {
//            // Unlock landscape view orientations for this view controller
//            return .allButUpsideDown;
//            }
//        }
//    
//        // Only allow portrait (standard behaviour)
//        return .portrait;
//    }
    
    private func topViewControllerWithRootViewController(rootViewController: UIViewController!) -> UIViewController? {
        if (rootViewController == nil) { return nil }
        if (rootViewController.isKind(of: UITabBarController.self)) {
            return topViewControllerWithRootViewController(rootViewController: (rootViewController as! UITabBarController).selectedViewController)
        } else if (rootViewController.isKind(of: UINavigationController.self)) {
            return topViewControllerWithRootViewController(rootViewController: (rootViewController as! UINavigationController).visibleViewController)
        } else if (rootViewController.presentedViewController != nil) {
            return topViewControllerWithRootViewController(rootViewController: rootViewController.presentedViewController)
        }
        return rootViewController
    }

    
}

