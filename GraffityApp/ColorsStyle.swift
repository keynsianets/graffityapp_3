//
//  Colors.swift
//  GraffityApp
//
//  Created by Alex Suvorov on 9/4/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit


extension UIColor {
    
    @nonobjc class var darkSkyBlue: UIColor {
        return UIColor(red: 59.0 / 255.0, green: 124.0 / 255.0, blue: 236.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var brownishGrey: UIColor {
        return UIColor(white: 95.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var white: UIColor {
        return UIColor(white: 1.0, alpha: 1.0)
    }
    
    @nonobjc class var brownGrey: UIColor {
        return UIColor(white: 151.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var veryLightBlue: UIColor {
        return UIColor(red: 227.0 / 255.0, green: 233.0 / 255.0, blue: 242.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var waterBlue: UIColor {
        return UIColor(red: 15.0 / 255.0, green: 129.0 / 255.0, blue: 224.0 / 255.0, alpha: 1.0)
    }
    
}
