//
//  ExifKeys.swift
//  PhotoExif
//
//  Created by Денис Марков on 4/1/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import Foundation

enum ExifKeys: String {
    case aperture = "ApertureValue"
    case iso = "ISOSpeedRatings"
    case focalLenght = "FocalLength"
    case focalLengthIn35 = "FocalLenIn35mmFilm"
    case flash = "Flash"
    case maxApperture = "MaxApertureValue"
    case dateTaken = "DateTimeOriginal"
    case editedDate = "DateTimeDigitized"
    case contrast = "Contrast"
    case customRendered = "CustomRendered"
    case flashpix = "FlashPixVersion"
    case digitalZoomRatio = "DigitalZoomRatio"
    case exposureBias = "ExposureBiasValue"
    case exposureMode = "ExposureMode"
    case exposureProgram = "ExposureProgram"
    case exposureTime = "ExposureTime"
    case whiteBalance = "WhiteBalance"
    case meteringMode = "MeteringMode"
    case sceneCaptureType = "SceneCaptureType"
    case sensingMethod = "SensingMethod"
    case sharpness = "Sharpness"
    case width = "PixelXDimension"
    case height = "PixelYDimension"
    case subjectDistanceRange = "SubjectDistRange"
}
