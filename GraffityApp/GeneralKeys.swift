//
//  GeneralKeys.swift
//  PhotoExif
//
//  Created by Денис Марков on 3/29/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import Foundation

enum GeneralKeys: String {
    case colorModel = "ColorModel"
    case dpiHeight = "DPIHeight"
    case dpiWidth = "DPIWidth"
    case depth = "Depth"
    case orientation = "Orientation"
    case profileName = "ProfileName"
}
