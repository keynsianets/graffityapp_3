//
//  MetadataTransform.swift
//  PhotoExif
//
//  Created by Денис Марков on 3/29/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import Foundation

class MetadataTransform {
    
    var parameters: [String: Any] = [:]
    
    init(data: [String: Any]) {
       // print("\n  -==================================================- \n")
      //  print(data)
     //   print("\n  -==================================================- \n")
        
        getMainData(data: data)
        getEXIFData(data: data)
        getTIFFData(data: data)
        getGPSData(data: data)
        
        
      
    }
    
    func getMainData(data: [String: Any]) {
        let colorModel = data[GeneralKeys.colorModel.rawValue] as? String
        mapKeyData(key: Strings.colorModel, data: colorModel)
        let dpiHeight = data[GeneralKeys.dpiHeight.rawValue] as? Int
        mapKeyData(key: Strings.dpiHeight, data: dpiHeight)
        let dpiWidth = data[GeneralKeys.dpiWidth.rawValue] as? Int
        mapKeyData(key: Strings.dpiWidth, data: dpiWidth)
        let depth = data[GeneralKeys.depth.rawValue] as? Int
        mapKeyData(key: Strings.depth, data: depth)
        if let orientation = data[GeneralKeys.orientation.rawValue] as? Int {
            var value = "Undefined"
            switch orientation {
            case 1:
                value = "top"
                break
            case 2:
                value = "top"
                break
            case 3:
                value = "bottom"
                break
            case 4:
                value = "bottom"
                break
            case 5:
                value = "left side"
                break
            case 6:
                value = "right side"
                break
            case 7:
                value = "right side"
                break
            case 8:
                value = "left side"
                break
            default:
                break
            }
            mapKeyData(key: Strings.orientation, data: value)
        } else {
            mapKeyData(key: Strings.orientation, data: Strings.noDataGeneral)
        }
        let profileName = data[GeneralKeys.profileName.rawValue] as? String
        mapKeyData(key: Strings.profileName, data: profileName)
    }
    
    func getEXIFData(data: [String: Any]) {
        if let exif = data["{Exif}"] as? [String: Any] {
            let aperture = exif["ApertureValue"] as? Double
            mapKeyData(key: "Aperture (F-number)", data: aperture?.rounded(toPlaces: 1))
            let iso = exif["ISOSpeedRatings"] as? NSArray
            let isoValue = iso?.firstObject as? Double
            mapKeyData(key: "ISO Sensitivity", data: isoValue)
            let focalLenght = exif["FocalLength"] as? Double
            mapKeyData(key: "Focal Length", data: focalLenght)
            let focalLengthIn35  = exif["FocalLenIn35mmFilm"] as? Double
            mapKeyData(key: "Focal Length in 35 mm", data: focalLengthIn35)
            let flash = exif["Flash"] as? Double
            mapKeyData(key: "Flash", data: flash)
            let maxApperture = exif["MaxApertureValue"] as? Double
            mapKeyData(key: "Aperture Max", data: maxApperture)
            let dateTaken = exif["DateTimeOriginal"] as? String
            mapKeyData(key: "Date Taken", data: dateTaken)
            let editedDate = exif["DateTimeDigitized"] as? String
            mapKeyData(key: "Edited Date", data: editedDate)
            if let contrast = exif["Contrast"] as? Int {
                var value = "Normal"
                switch contrast {
                case 0:
                    value = "Normal"
                    break
                case 1:
                    value = "Soft"
                    break
                case 2:
                    value = "Hard"
                    break
                default:
                    break
                }
                mapKeyData(key: "Contrast", data: value)
            } else {
                mapKeyData(key: "Contrast", data: Strings.noDataGeneral)
            }
            if let customRendered = exif["CustomRendered"] as? Int {
                var value = "normal process"
                if customRendered == 1 {
                    value = "custom process"
                }
                mapKeyData(key: "Custom Rendered", data: value)
            } else {
                mapKeyData(key: "Custom Rendered", data: Strings.noDataGeneral)
            }
            if let flashpix = exif["FlashPixVersion"] as? NSArray {
                var value = ""
                for version in flashpix {
                    if let versionDouble = version as? Double {
                        value += "\(versionDouble.rounded(toPlaces: 1)), "
                    }
                }
                if value.count > 2 {
                    value = String(value.dropLast().dropLast())
                }
                mapKeyData(key: "FlashpixVersion", data: value)
            } else {
                mapKeyData(key: "FlashpixVersion", data: Strings.noDataGeneral)
            }
            let digitalZoomRation = exif["DigitalZoomRatio"] as? Double
            mapKeyData(key: "Digital Zoom Ratio", data: digitalZoomRation?.rounded(toPlaces: 2))
            let exposureBias = exif["ExposureBiasValue"] as? Double
            mapKeyData(key: "Exposure Bias Value", data: exposureBias?.rounded(toPlaces: 2))
            if let exposureMode = exif["ExposureMode"] as? Int {
                var value = "auto exposure"
                if exposureMode == 1 {
                    value = "manual exposure"
                }
                if exposureMode == 2 {
                    value = "auto bracketing"
                }
                mapKeyData(key: "Exposure Mode", data: value)
            } else {
                mapKeyData(key: "Exposure Mode", data: Strings.noDataGeneral)
            }
            if let exposureProgram = exif["ExposureProgram"] as? Int {
                var value = "Not defined"
                switch exposureProgram {
                case 0:
                    value = "Not defined"
                    break
                case 1:
                    value = "Manual"
                    break
                case 2:
                    value = "Normal program"
                    break
                case 3:
                    value = "Aperture priority"
                    break
                case 4:
                    value = "Shutter priority"
                    break
                case 5:
                    value = "Creative program (biased toward depth of field)"
                    break
                case 6:
                    value = "Action program (biased toward fast shutter speed)"
                    break
                case 7:
                    value = "Portrait mode (for closeup photos with the background out of focus)"
                    break
                case 8:
                    value = "Landscape mode (for landscape photos with the background in focus)"
                    break
                default:
                    break
                }
                mapKeyData(key: "Exposure Program", data: value)
            } else {
                mapKeyData(key: "Exposure Program", data: Strings.noDataGeneral)
            }
            let exposureTime = exif["ExposureTime"] as? Double
            mapKeyData(key: "Exposure Time", data: exposureTime)
            if let whiteBalance = exif["WhiteBalance"] as? Int {
                var value = "auto white balance"
                if whiteBalance == 1 {
                    value = "manual white balance"
                }
                mapKeyData(key: "White Balance", data: value)
            } else {
                mapKeyData(key: "White Balance", data: Strings.noDataGeneral)
            }
            if let meteringMode = exif["MeteringMode"] as? Int {
                var value = "Unknown"
                switch meteringMode {
                case 0:
                    value = "Unknown"
                    break
                case 1:
                    value = "Average"
                    break
                case 2:
                    value = "CenterWeightedAverage"
                    break
                case 3:
                    value = "Spot"
                    break
                case 4:
                    value = "MultiSpot"
                    break
                case 5:
                    value = "Pattern"
                    break
                case 6:
                    value = "Partial"
                    break
                case 255:
                    value = "other"
                    break
                default:
                    break
                }
                mapKeyData(key: "Metering Mode", data: value)
            } else {
                mapKeyData(key: "Metering Mode", data: Strings.noDataGeneral)
            }
            if let sceneCaptureType = exif["SceneCaptureType"] as? Int {
                var value = "landscape"
                switch sceneCaptureType {
                case 1:
                    value = "landscape"
                    break
                case 2:
                    value = "portrait"
                    break
                case 3:
                    value = "night scene"
                    break
                default:
                    break
                }
                mapKeyData(key: "Scene Capture Type", data: value)
            } else {
                mapKeyData(key: "Scene Capture Type", data: Strings.noDataGeneral)
            }
            if let sensingMethod = exif["SensingMethod"] as? Int {
                var value = "Not defined"
                switch sensingMethod {
                case 1:
                    value = "Not defined"
                    break
                case 2:
                    value = "One-chip color area sensor"
                    break
                case 3:
                    value = "Two-chip color area sensor"
                    break
                case 4:
                    value = "Three-chip color area sensor"
                    break
                case 5:
                    value = "Color sequential area sensor"
                    break
                case 7:
                    value = "Trilinear sensor"
                    break
                case 8:
                    value = "Color sequential linear sensor"
                    break
                default:
                    break
                }
                mapKeyData(key: "Sensing Method", data: value)
            } else {
                mapKeyData(key: "Sensing Method", data: Strings.noDataGeneral)
            }
            if let sharpness = exif["Sharpness"] as? Int {
                var value = "Normal"
                switch sharpness {
                case 0:
                    value = "Normal"
                    break
                case 1:
                    value = "Soft"
                    break
                case 2:
                    value = "Hard"
                    break
                default:
                    break
                }
                mapKeyData(key: "Sharpness", data: value)
            } else {
                mapKeyData(key: "Sharpness", data: Strings.noDataGeneral)
            }
            if let width = exif["PixelXDimension"] as? Int, let height = exif["PixelYDimension"] as? Int {
                mapKeyData(key: "Resolution", data: Float(width * height) / 1000000)
                mapKeyData(key: "Aspect Ratio", data: width / height)
            } else {
                mapKeyData(key: "Resolution", data: Strings.noDataGeneral)
                mapKeyData(key: "Aspect Ratio", data: Strings.noDataGeneral)
            }
            if let subjectDistanceRange = exif["SubjectDistRange"] as? Int {
                var value = "Unknown"
                switch subjectDistanceRange {
                case 0:
                    value = "Unknown"
                    break
                case 1:
                    value = "Macro"
                    break
                case 2:
                    value = "Close view"
                    break
                case 3:
                    value = "Distant view"
                    break
                default:
                    break
                }
                mapKeyData(key: "Subject Distance Range", data: value)
            } else {
                mapKeyData(key: "Subject Distance Range", data: Strings.noDataGeneral)
            }
            if let colorSpace = exif["ColorSpace"] as? Int {
                var value = "sRGB"
                if colorSpace != 1 {
                    value = "Uncalibrated"
                }
                mapKeyData(key: "Color Space", data: value)
            } else {
                mapKeyData(key: "Color Space", data: Strings.noDataGeneral)
            }
            if let gainControl = exif["GainControl"] as? Int {
                var value = "None"
                switch gainControl {
                case 0:
                    value = "None"
                    break
                case 1:
                    value = "Low gain up"
                    break
                case 2:
                    value = "High gain up"
                    break
                case 3:
                    value = "Low gain down"
                    break
                case 4:
                    value = "High gain down"
                    break
                default:
                    break
                }
                mapKeyData(key: "Gain Control", data: value)
            } else {
                mapKeyData(key: "Gain Control", data: Strings.noDataGeneral)
            }
            if let lightSource = exif["LightSource"] as? Int {
                var value = "Unknown"
                switch lightSource {
                case 0:
                    value = "Unknown"
                    break
                case 1:
                    value = "Daylight"
                    break
                case 2:
                    value = "Fluorescent"
                    break
                case 3:
                    value = "Tungsten (incandescent light)"
                    break
                case 4:
                    value = "Flash"
                    break
                case 9:
                    value = "Fine weather"
                    break
                case 10:
                    value = "Cloudy weather"
                    break
                case 11:
                    value = "Shade"
                    break
                case 12:
                    value = "Daylight fluorescent (D 5700 - 7100K)"
                    break
                case 13:
                    value = "Day white fluorescent (N 4600 - 5400K)"
                    break
                case 14:
                    value = "Cool white fluorescent (W 3900 - 4500K)"
                    break
                case 15:
                    value = "White fluorescent (WW 3200 - 3700K)"
                    break
                case 17:
                    value = "Standard light A"
                    break
                case 18:
                    value = "Standard light B"
                    break
                case 19:
                    value = "Standard light C"
                    break
                case 20:
                    value = "D55"
                    break
                case 21:
                    value = "D65"
                    break
                case 22:
                    value = "D75"
                    break
                case 23:
                    value = "D50"
                    break
                case 24:
                    value = "ISO studio tungsten"
                    break
                case 255:
                    value = "Other light source"
                    break
                default:
                    break
                }
                mapKeyData(key: "Light Source", data: value)
            } else {
                mapKeyData(key: "Light Source", data: Strings.noDataGeneral)
            }
            if let saturation = exif["Saturation"] as? Int {
                var value = "Normal"
                switch saturation {
                case 0:
                    value = "Normal"
                    break
                case 1:
                    value = "Low saturation"
                    break
                case 2:
                    value = "High saturation"
                    break
                default:
                    break
                }
                mapKeyData(key: "Saturation", data: value)
            } else {
                mapKeyData(key: "Saturation", data: Strings.noDataGeneral)
            }
            let lensMake = exif["LensMake"] as? String
            mapKeyData(key: "Lens Make", data: lensMake)
            let lensModel = exif["LensModel"] as? String
            mapKeyData(key: "Lens Model", data: lensModel)
            let cameraSerialNumber = exif["SerialNumber"] as? String
            mapKeyData(key: "Camera Serial Number", data: cameraSerialNumber)
        } else {
            mapKeyData(key: "EXIF", data: Strings.noDataGeneral)
        }
    }
    
    func getTIFFData(data: [String: Any]) {
        if let tiff = data["{TIFF}"] as? [String: Any] {
            let artist = tiff["Artist"] as? String
            mapKeyData(key: "Artist", data: artist)
            let copyright = tiff["Copyright"] as? String
            mapKeyData(key: "Copyright", data: copyright)
            let make = tiff["Make"] as? String
            mapKeyData(key: "Make", data: make)
            let model = tiff["Model"] as? String
            mapKeyData(key: "Model", data: model)
            let software = tiff["Software"] as? String
            mapKeyData(key: "Software", data: software)
        } else {
            mapKeyData(key: "TIFF", data: Strings.noDataGeneral)
        }
    }
    
    func getGPSData(data: [String: Any]) {
        if let gps = data["{GPS}"] as? [String: Any] {
            let speed = gps["Speed"] as? Double
            mapKeyData(key: "Speed", data: speed?.rounded(toPlaces: 1))
            let alitude = gps["Altitude"] as? Double
            mapKeyData(key: "Altitude", data: alitude?.rounded(toPlaces: 2))
            let longitude = gps["Longitude"] as? Double
            mapKeyData(key: "Longitude", data: longitude)
            let latitude = gps["Latitude"] as? Double
            mapKeyData(key: "Latitude", data: latitude)
            let longitudeRef = gps["LongitudeRef"] as? String
            mapKeyData(key: "LongitudeRef", data: longitudeRef)
            let latitudeRef = gps["LatitudeRef"] as? String
            mapKeyData(key: "LatitudeRef", data: latitudeRef)
            let imgDirection = gps["ImgDirection"] as? Double
            mapKeyData(key: "ImgDirection", data: imgDirection)
            let imgDirectionRef = gps["ImgDirectionRef"] as? String
            mapKeyData(key: "ImgDirectionRef", data: imgDirectionRef)
        } else {
            mapKeyData(key: "GPS", data: Strings.noDataGeneral)
        }
    }
    
    func mapKeyData(key: String, data: Any?) {
        if let value = data {
            if let ddd = data as? String,   ddd == Strings.noDataGeneral {return}
            
            
            parameters[key] = value
        } else {
          //  parameters[key] = Strings.noDataGeneral
        }
        
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

