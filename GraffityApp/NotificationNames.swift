//
//  NotificationNames.swift
//  GraffityApp
//
//  Created by Alex Suvorov on 9/4/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let dissmis_tab_bar = Notification.Name("dissmis_tab_bar")
}
