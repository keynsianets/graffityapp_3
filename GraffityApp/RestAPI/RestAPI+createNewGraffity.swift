//
//  RestAPI+createNewGraffity.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/16/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import Foundation

import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import SwiftyJSON
import CoreLocation
//import ImageIO


extension RestAPI {
    
    func createNewGraffity(data: ImageToServer, image: UIImage, metadata: [String : Any]?, callback: @escaping (_ isOK: Bool) -> Void) {
        let url = Settings.SERVER_URL + "dev/graffitis"
        var parameters: [String : Any] = [:]
        parameters["title"] = data.title
        parameters["description"] = data.description
        parameters["formatted_address"] = data.address
        parameters["source"] = data.source
        parameters["tags"] = data.tags
        var exifData : [String : Any] = [:]
        
        if let metadata = metadata {
         //   print(MetadataTransform(data: metadata).parameters)
            exifData = MetadataTransform(data: metadata).parameters
        }
       
        if let lat = exifData["Latitude"], let lng = exifData["Longitude"] {
            exifData["GPS coordinates"] = [lat, lng]
            parameters["location"] = ["type": "Point", "coordinates": [lat, lng], "lat": lat, "lng": lng ]
        } else {
            exifData["GPS coordinates"] = [data.location?.lat, data.location?.lng]
            if let lat = data.location?.lat, let lng = data.location?.lng {
                parameters["location"] = ["type": "Point", "coordinates": [lat, lng], "lat": lat, "lng": lng ]
            }
        }
        parameters["exif_data"] = exifData
        print("----------------------------------");
        print("----------------------------------");
        print("----------------------------------");
            print(parameters)
       print("----------------------------------");
        
     //    return
        
        let resizedImage = resizeImage(image: image, targetSize: CGSize(width: image.size.width/2, height: image.size.height/2))
        let commpression : CGFloat = 0.9
        guard let imageData: Data = resizedImage.jpegData(compressionQuality: commpression) else { return }
      
        headers["Authorization"] = "Bearer \(UserDefaults(suiteName: "group.graffititagr")?.string(forKey: "kUserProfile_token") ?? "" )"
    
        
         let request = Alamofire.request(url, method: .post, parameters: parameters, encoding: GZIPEncoding(), headers: headers).validate().responseObject{ (response: DataResponse<Image>) in
            guard let responseCode = response.response?.statusCode else {
                callback(false)
                return
            }
        debugPrint(parameters, "parameters to server")
            if responseCode == 201 {
                guard let imageUploadId = response.result.value?.id else { return }
                debugPrint("id",  imageUploadId)
                let imageUploadUrl = Settings.SERVER_URL + "dev/graffitis/\(imageUploadId)/upload/"
                Alamofire.upload(multipartFormData: { (multipartFormData) in
                    multipartFormData.append(imageData, withName: "file", fileName: "file.jpg", mimeType: "image/jpeg")
                    
                }, to: imageUploadUrl, method : .post, headers: self.uncompressedHeaders)
                { (result) in
                    switch result {
                    case .success(let upload, _, _):
                        
                        upload.uploadProgress(closure: { (Progress) in
                            debugPrint("Upload Progress: \(Progress.fractionCompleted)")
                        })
                        
                        upload.response { response in
                            // debugPrint(response.error)
                            debugPrint("-===== Image uploaded : ", NSDate(), "=====-")
                            if response.error != nil {
                                callback(false)
                                return
                            }
                            callback(true)
                            
                        }
                        
                    case .failure(let encodingError):
                        //self.delegate?.showFailAlert()
                        callback(false)
                        debugPrint(encodingError)
                    }
            }
            } else {
                
                if let data = response.data {
                    do {
                        let json = try JSON(data: data)
                        print(json)
                        callback(false)
                        return
                    } catch let error {
                        debugPrint(error)
                        callback(false)
                        return
                    }
                }
                
                callback(false)
            }
    }
        print("---------------------------------")
        print(request)
        
    }

}

func getMetaData(forImage image: UIImage) {
    guard let data = image.jpegData(compressionQuality: 1),
        let source = CGImageSourceCreateWithData(data as CFData, nil) else { return}
    
    if let type = CGImageSourceGetType(source) {
        print("type: \(type)")
    }
    
    if let properties = CGImageSourceCopyProperties(source, nil) {
        print("properties - \(properties)")
    }
    
    let count = CGImageSourceGetCount(source)
    print("count: \(count)")
    
    for index in 0..<count {
        if let metaData = CGImageSourceCopyMetadataAtIndex(source, index, nil) {
            print("all metaData[\(index)]: \(metaData)")
            
            let typeId = CGImageMetadataGetTypeID()
            print("metadata typeId[\(index)]: \(typeId)")
            
            
            if let tags = CGImageMetadataCopyTags(metaData) as? [CGImageMetadataTag] {
                
                print("number of tags - \(tags.count)")
                
                for tag in tags {
                    
                    if let name = CGImageMetadataTagCopyName(tag) {
                        print("name: \(name)")
                    }
                    if let value = CGImageMetadataTagCopyValue(tag) {
                        print("value: \(value)")
                    }
                    if let prefix = CGImageMetadataTagCopyPrefix(tag) {
                        print("prefix: \(prefix)")
                    }
                    if let namespace = CGImageMetadataTagCopyNamespace(tag) {
                        print("namespace: \(namespace)")
                    }
                    if let qualifiers = CGImageMetadataTagCopyQualifiers(tag) {
                        print("qualifiers: \(qualifiers)")
                    }
                    print("-------")
                }
            }
        }
        
        if let properties = CGImageSourceCopyPropertiesAtIndex(source, index, nil) {
            print("properties[\(index)]: \(properties)")
        }
    }
}

func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
    let size = image.size
    
    let widthRatio  = targetSize.width  / size.width
    let heightRatio = targetSize.height / size.height
    
    // Figure out what our orientation is, and use that to form the rectangle
    var newSize: CGSize
    if(widthRatio > heightRatio) {
        newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
    } else {
        newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
    }
    
    // This is the rect that we've calculated out and this is what is actually used below
    let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
    
    // Actually do the resizing to the rect using the ImageContext stuff
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    image.draw(in: rect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage!
}



class Images : Mappable {
    var count : Int?
    var next : String?
    var previous : String?
    var results: [Image]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        count           <- map["count"]
        next            <- map["next"]
        previous        <- map["previous"]
        results         <- map["results"]
    }
}

class Image: Mappable, Equatable {
    var id: String?
    var title: String?
    var description: String?
    var reporter: String?
    var address: String?
    var location: Location?
    var image: String?
    var status : String?
    var createdAt : String?
    var updatedAt : String?
    var opened = false
    
    static func == (lhs: Image, rhs: Image) -> Bool{
        return lhs.id == rhs.id
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id                <- map["id"]
        title             <- map["title"]
        description       <- map["description"]
        reporter          <- map["reporter"]
        address           <- map["address"]
        address           <- map["formatted_address"]
        location          <- map["location"]
        image             <- map["image"]
        status            <- map["status"]
        createdAt         <- map["created_at"]
        updatedAt         <- map["updated_at"]
    }
    
    
}

class ImageToServer: Mappable {
    var title: String?
    var description: String?
    var address: String?
    var location: Location?
    var source = "LIVE"
    var tags = ["mcd", "samer"]
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        title             <- map["title"]
        description       <- map["description"]
        address           <- map["address"]
        location          <- map["location"]
        source            <- map["source"]
        tags              <- map["tags"]
    }
    
    init() {
        
    }
    
    convenience init(title: String?, location: Location?, source: String) {
        self.init()
        self.title = title
        self.location = location
        self.description = "no description"
        self.address = "no address"
        self.source = source
        self.tags = ["mcd"]
    }
    
}

class Location: Mappable {
    var type: String?
    var lat: Double?
    var lng: Double?
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        type              <- map["type"]
        lat               <- map["lat"]
        lng               <- map["lng"]
    }
    
    init() {
        
    }
    
    convenience init(coordinates: CLLocationCoordinate2D) {
        self.init()
        self.lat = coordinates.latitude
        self.lng = coordinates.longitude
        self.type = "Point"
    }
    
    
}


extension UIImage {
    func getExifData() -> CFDictionary? {
        var exifData: CFDictionary? = nil
        if let data = self.jpegData(compressionQuality: 1.0) {
            data.withUnsafeBytes {(bytes: UnsafePointer<UInt8>)->Void in
                if let cfData = CFDataCreate(kCFAllocatorDefault, bytes, data.count) {
                    if let source = CGImageSourceCreateWithData(cfData, nil) {
                        exifData = CGImageSourceCopyPropertiesAtIndex(source, 0, nil)
                    }
                }
            }
        }
        return exifData
    }
}
