//
//  RestAPI+getGraffities.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/16/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import SwiftyJSON

extension RestAPI {
    
    func getGraffities(page: Int? = 0, page_size: Int?, callback: @escaping (_ isOK: Bool, [Image]?) -> Void) {
        
        let productsURL = Settings.SERVER_URL + "dev/graffitis"
        
        var parameters : [String : Any] = [:]
        
        if let page_size = page_size {
            parameters["limit"] = page_size
            if let page = page {
                parameters["offset"] = page * page_size
            }
        }
        //debugPrint(productsURL, "url")
        Alamofire.request(productsURL, method: .get, parameters: parameters, headers: headers).validate().responseObject{ (response: DataResponse<Images>) in
            
            guard response.result.isSuccess else {
                if let stausCode = response.response?.statusCode {
                    if stausCode == 404 {
                        callback(true, nil)
                        return
                    }
                }
                callback(false, nil)
                return
            }
            
            if let value = response.result.value {
                callback(true, value.results)
            } else {
                callback(false, nil)
            }
        }
    }
}
