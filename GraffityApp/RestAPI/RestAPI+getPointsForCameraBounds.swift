//
//  RestAPI+getPointsForCameraBounds.swift
//  GraffityApp
//
//  Created by Denis Markov on 9/20/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

extension RestAPI {
    
    func getPointsForCameraBounds(box: String?, page: Int = 0, page_size: Int = 25, callback: @escaping (_ isOK: Bool, [Image]?) -> Void) {
        
        let productsURL = Settings.SERVER_URL + "dev/graffitis/feed/"
        
        var parameters : [String : Any] = [:]
        
        var loadedCount = 0
        
        parameters["include_fields"] = "id,title,location,adress,formatted_address,image,status"
        parameters["limit"] = page_size
        parameters["offset"] = page * page_size
        
        if let box = box {
            parameters["in_bbox"] = box
        }
        
        var points = [Image]()
        
        Alamofire.request(productsURL, method: .get, parameters: parameters, headers: headers).validate().responseObject{ (response: DataResponse<Images>) in
//            self.printResponse(from: response, parameters: parameters)
            guard response.result.isSuccess else {
                if let stausCode = response.response?.statusCode {
                    if stausCode == 404 {
                        callback(true, nil)
                        return
                    }
                }
                callback(false, nil)
                return
            }
            
            if let value = response.result.value, let result = value.results {
                    loadedCount = (page + 1) * page_size
                    if loadedCount < value.count ?? 0 {
                        points += result
                        RestAPI().getPointsForCameraBounds(box: box, page: page + 1, page_size: page_size) { (isOk, additionalPoints) in
//                            debugPrint("load points in box - box: ", box, "points: ", points)
                            if isOk, let additionalPoints = additionalPoints {
                                points += additionalPoints
                            }
                            callback(true, points)
                        }
                    } else {
                        callback(true, value.results)
                }
                
            } else {
                callback(false, nil)
            }
        }
    }
    
    private func printResponse(from response: DataResponse<Images>,
                               parameters: [String: Any]?) {
        print("request path: \(response.request?.url?.absoluteString ?? "")")
        print("request method: \(response.request?.httpMethod ?? "")")
        print("request headers: \(response.request?.allHTTPHeaderFields ?? [:])")
        print("request parameters: \(parameters ?? [:])")
        print("status code: \(response.response?.statusCode ?? 0)")
        guard let data = response.data else {
            return
        }
        do {
            let parsedData = try JSONSerialization.jsonObject(with: data, options: [])
            print("response: \(parsedData)")
        } catch {
            let responseString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            print("error: \(responseString ?? "")")
        }
    }
}

