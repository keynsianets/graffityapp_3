//
//  RestAPI+getUserInfo.swift
//  GraffityApp
//
//  Created by Denis Markov on 8/2/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import Foundation
//import SwiftyJSON
import Alamofire
import ObjectMapper

extension RestAPI {
    
    func getUserInfo(callback: @escaping (_ isOK: Bool, _ user: UserServerInfo?) -> Void) {
        let registerURL = Settings.SERVER_URL + "dev/users/me/"
        debugPrint(headers, "getUserInfo headers")
        Alamofire.request(registerURL, method: .get, headers: headers).validate().responseObject{ (response: DataResponse<UserServerInfo>) in
            
            guard response.result.isSuccess else {
                debugPrint("Error: \(String(describing: response.result))")
                callback(false, nil)
                return
            }
            
            if let value = response.result.value {
                callback(true, value)
            } else {
                callback(false, nil)
            }
        }
    }
}


class UserServerInfo : NSObject, Mappable {
    
    var id: String?
    var username: String?
    var email: String?
    var full_name: String?
        
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                  <- map["id"]
        username            <- map["username"]
        email               <- map["email"]
        full_name           <- map["full_name"]
    }
        
        
}
