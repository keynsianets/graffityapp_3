//
//  RestAPI+removeGraffity.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/16/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import SwiftyJSON
import CoreLocation

extension RestAPI {
    
    func removeGraffity(data: ImageToServer, callback: @escaping (_ isOK: Bool) -> Void) {
        let url = Settings.SERVER_URL + "dev/graffitis"
        
        
        let parameters: [String : Any] = data.toJSON()
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).response(completionHandler: {(response) in
            dump(response.response?.statusCode, name: "response")
            guard let responseCode = response.response?.statusCode else {
                callback(false)
                return
            }
            callback(responseCode == 201)
        })
    }
    
}
