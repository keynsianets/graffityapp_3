//
//  RestAPI.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/16/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import SwiftyJSON

class RestAPI {
    var urlSession: URLSession!
//    var defaultHeaders = [
//        "Content-Type":"application/json",
//        "Accept":"application/json",
// //       "Authorization": "Basic \("admin:graffiti_admin".toBase64())"
//    ]
    var headers = [
        "Content-Type":"application/json",
        "Accept":"application/json",
        "Authorization": "Bearer \(UserProfile.shared.token ?? "" )",
        "Content-Encoding": "gzip",
        "App-Version": Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
     ]
    
    var uncompressedHeaders = [
       "Content-Type":"application/json",
       "Accept":"application/json",
       "Authorization": "Bearer \(UserProfile.shared.token ?? "" )",
       "App-Version": Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    ]
    
    var unauthHeader = [
       "Content-Type":"application/json",
       "Accept": "application/json",
       "Content-Encoding": "gzip",
       "App-Version": Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    ]
    //Authorization: Basic dGVzdGFwcHVzZXI6Nzg5NDU2dzc=
    //Bearer token_here
    
    
}

