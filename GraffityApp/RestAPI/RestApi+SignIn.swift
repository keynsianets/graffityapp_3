//
//  RestApi+SignIn.swift
//  GraffityApp
//
//  Created by Alex Suvorov on 5/3/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import ObjectMapper

extension RestAPI {
    
    func signIn(userName: String, password: String, callback: @escaping (_ isOK: Bool, _ token: String?) -> Void) {
        let registerURL = Settings.SERVER_URL + "dev/login/"
        
        let parameters = [
            "username": userName,
            "password": password,
        ]
        
        Alamofire.request(registerURL, method: .post, parameters: parameters, encoding: GZIPEncoding(), headers: unauthHeader).validate().responseObject{ (response: DataResponse<Token>) in
            
            guard response.result.isSuccess else {
                debugPrint("Error: \(String(describing: response.result))")
                callback(false, nil)
                return
            }
            
            if let value = response.result.value {
                callback(true, value.token)
            } else {
                callback(false, nil)
            }
        }
    }
    
    
    func signIn(facebookToken: String, callback: @escaping (_ isOK: Bool, _ token: String?) -> Void) {
        let registerURL = Settings.SERVER_URL + "dev/login/"
        let parameters = [
            "fb_access_token": facebookToken
        ]
        Alamofire.request(registerURL, method: .post, parameters: parameters, encoding: GZIPEncoding(), headers: unauthHeader).validate().responseObject{ (response: DataResponse<Token>) in
            guard response.result.isSuccess else {
                if let data = response.data {
                    do {
                        let json = try JSON(data: data)
                       print(json)
                         debugPrint("Error: \(String(describing: response.result))")
                        callback(false, nil)
                        return
                    } catch let error {
                        debugPrint(error)
                         debugPrint("Error: \(String(describing: response.result))")
                        callback(false, nil)
                        return
                    }
                }
                
                debugPrint("Error: \(String(describing: response.result))")
                callback(false, nil)
                return
            }
            
            if let value = response.result.value {
                callback(true, value.token)
            } else {
                callback(false, nil)
            }
        }
    }
}





class Token: Mappable {
    var token : String?
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        token         <- map["token"]
    }
    
}

