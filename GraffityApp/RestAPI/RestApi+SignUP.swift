//
//  RestApi+SignUP.swift
//  GraffityApp
//
//  Created by Suvorov on 5/1/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import ObjectMapper

extension RestAPI {
    func register(userName: String, email: String, password: String, full_name: String, callback: @escaping (_ isOK: Bool, _ error: RegisterError?) -> Void) {
        let registerURL = Settings.SERVER_URL + "dev/users/"
      
        
        let parameters = [
            "username": userName,
            "email": email,
            "password": password,
            "full_name": full_name
            
        ]
        
        Alamofire.request(registerURL, method: .post, parameters: parameters, encoding: GZIPEncoding(), headers: unauthHeader).validate().responseObject{ (response: DataResponse<UserInfo>) in
            guard response.result.isSuccess else {
                debugPrint("Error: \(String(describing: response.result))")
                self.printResponse(from: response, parameters: parameters)
                if let data = response.data {
                    do {
                        let jsonNew = try JSONSerialization.jsonObject(with: data, options: [.mutableContainers]) as? [String : Any]
                        debugPrint(jsonNew, "jsonNew")
                        let json = try JSON(data: data)
                        debugPrint(json, "json")
                        let err = RegisterError(json: json)
                        debugPrint(err.getCommonError())
                        callback(false, RegisterError(json: json))
                        return
                    } catch let error {
                        debugPrint(error)
                        callback(false, nil)
                        return
                    }
                }
                
                
                callback(false, nil)
                return
            }
            
            if let value = response.result.value {
                UserProfile.shared.id = value.id
                callback(true, nil)
            } else {
                callback(false, nil)
            }
        }
    }
    
    func register(userName: String, email: String, fb_access_token: String, full_name: String, callback: @escaping (_ isOK: Bool, _ error: RegisterError?) -> Void) {
        let registerURL = Settings.SERVER_URL + "dev/users"
        
        
        let parameters = [
            "username": userName,
            "email": email,
            "fb_access_token": fb_access_token,
            "full_name": full_name
            
        ]
        
        
        Alamofire.request(registerURL, method: .post, parameters: parameters, encoding: GZIPEncoding(), headers: unauthHeader).validate().responseObject{ (response: DataResponse<UserInfo>) in
            guard response.result.isSuccess else {
                debugPrint("Error: \(String(describing: response.result))")
                
                if let data = response.data {
                    do {
                        let json = try JSON(data: data)
                        debugPrint(json, "login error response")
                        callback(false, RegisterError(json: json))
                        return
                    } catch let error {
                        debugPrint(error)
                        callback(false, nil)
                        return
                    }
                }
                
                
                callback(false, nil)
                return
            }
            
            if let value = response.result.value {
                UserProfile.shared.id = value.id
                callback(true, nil)
            } else {
                callback(false, nil)
            }
        }
    }
    
    private func printResponse(from response: DataResponse<UserInfo>,
                               parameters: [String: Any]?) {
        print("request path: \(response.request?.url?.absoluteString ?? "")")
        print("request method: \(response.request?.httpMethod ?? "")")
        print("request headers: \(response.request?.allHTTPHeaderFields ?? [:])")
        print("request parameters: \(parameters ?? [:])")
        print("status code: \(response.response?.statusCode ?? 0)")
        guard let data = response.data else {
            return
        }
        do {
            let parsedData = try JSONSerialization.jsonObject(with: data, options: [])
            print("response: \(parsedData)")
        } catch {
            let responseString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            print("error: \(responseString ?? "")")
        }
    }
}



class RegisterError {
    var country : String?
    var username : String?
    var email :  String?
    var password : String?
    var first_name : String?
    var last_name : String?
    init(json : JSON) {
        username   = json["username"][0].string
        country    = json["country"][0].string
        email      = json["email"][0].string
        password   = json["password"][0].string
        first_name = json["first_name"][0].string
        last_name  = json["last_name"][0].string
    }
    
    func getCommonError() -> String {
        var commonError = ""
        let mirrored_object = Mirror(reflecting: self)
        for (_, attr) in mirrored_object.children.enumerated() {
            if let value = attr.value as? String {
                commonError = commonError + "\n" + value
            }
        }
        return commonError
    }
}


class UserInfo: Mappable {
    var id : String?
    var email : String?
    var username : String?
    var full_name : String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id         <- map["id"]
        email      <- map["email"]
        username   <- map["username"]
        full_name  <- map["full_name"]
        
    }
    
}
