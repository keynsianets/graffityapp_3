//
//  RestApi+updateUserInfo.swift
//  GraffityApp
//
//  Created by Denis Markov on 12/19/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

extension RestAPI {
    func editUser(userName: String?, email: String?, password: String?, callback: @escaping (_ isOK: Bool, _ error: String?) -> Void) {
        let registerURL = Settings.SERVER_URL + "dev/users/"
        
        
        let parameters = [
            "username": userName,
            "email": email,
            "password": password
        ]
        
        if parameters.count < 1 {
            callback(false, nil)
            return
        }
        Alamofire.request(registerURL, method: .patch, parameters: parameters, encoding: GZIPEncoding(), headers: headers).validate().responseObject{ (response: DataResponse<UserInfo>) in
            guard response.result.isSuccess else {
                debugPrint("Error: \(String(describing: response.result))")
//                self.printResponse(from: response, parameters: parameters)
                if let data = response.data {
                    do {
                        let jsonNew = try JSONSerialization.jsonObject(with: data, options: [.mutableContainers]) as? [String : Any]
                        debugPrint(jsonNew, "jsonNew")
                        let json = try JSON(data: data)
                        debugPrint(json, "json")
                        let err = RegisterError(json: json)
                        debugPrint(err.getCommonError())
                        callback(false, err.getCommonError())
                        return
                    } catch let error {
                        debugPrint(error)
                        callback(false, error.localizedDescription)
                        return
                    }
                }
                
                
                callback(false, nil)
                return
            }
            
            if let value = response.result.value {
                UserProfile.shared.id = value.id
                callback(true, nil)
            } else {
                callback(false, Strings.somethingWentWrong)
            }
        }
    }
}
