//
//  UserProfile.swift
//  GraffityApp
//
//  Created by Suvorov on 5/1/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import Foundation


fileprivate struct Constants {
    static let kUserProfile_username = "kUserProfile_username"
    static let kUserProfile_email = "kUserProfile_email"
    static let kUserProfile_password = "kUserProfile_kUserProfile_password"
    static let kUserProfile_push_token = "kUserProfile_push_token"
    static let kUserProfile_first_name = "kUserProfile_first_name"
    static let kUserProfile_last_name = "kUserProfile_last_name"
    static let kUserProfile_full_name = "kUserProfile_full_name"
    static let kUserProfile_country = "kUserProfile_country"
    static let kUserProfile_token = "kUserProfile_token"
    static let kUserProfile_fb_token = "kUserProfile_fb_token"
    static let kUserProfile_countOfLaunches = "kUserProfile_countOfLaunches"
    static let kUserProfile_id = "kUserProfile_id"
    static let kUserProfile_needToShowWelcomeVC = "kUserProfile_needToShowWelcomeVC"

}



class UserProfile {
    
    static let shared = UserProfile()
    
    var notNeedToShowWelcomeScreens : Bool {
        set {
            UserDefaults.standard.set(newValue, forKey: Constants.kUserProfile_needToShowWelcomeVC)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.bool(forKey: Constants.kUserProfile_needToShowWelcomeVC)
        }
        
    }
    
    var token : String?  {
        set {
            let userDefaults = UserDefaults(suiteName: "group.graffititagr")
            userDefaults?.set(newValue, forKey: Constants.kUserProfile_token)
            userDefaults?.synchronize()
            updateUserInfo()
        }
        get {
            let userDefaults = UserDefaults(suiteName: "group.graffititagr")
            if let token = userDefaults?.string(forKey: Constants.kUserProfile_token) {
                print(token)
                return token
            }
            return nil
        }
    }
    
    var fb_token : String?  {
        set {
            UserDefaults.standard.set(newValue, forKey: Constants.kUserProfile_fb_token)
            UserDefaults.standard.synchronize()
        }
        get {
            if let token = UserDefaults.standard.string(forKey: Constants.kUserProfile_fb_token) {
                return token
            }
            
            return nil
        }
    }
    
    var id : String?  {
        set {
            UserDefaults.standard.set(newValue, forKey: Constants.kUserProfile_id)
            UserDefaults.standard.synchronize()
        }
        get {
            if let token = UserDefaults.standard.string(forKey: Constants.kUserProfile_id) {
                return token
            }
            
            return nil
        }
    }
    
    var count_of_launches : Int? {
        set {
            UserDefaults.standard.set(newValue, forKey: Constants.kUserProfile_countOfLaunches)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.integer(forKey: Constants.kUserProfile_countOfLaunches)
        }
    }
    
    var username : String?  {
        set {
            UserDefaults.standard.set(newValue, forKey: Constants.kUserProfile_username)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: Constants.kUserProfile_username)
        }
    }
    
    var email : String?  {
        set {
            UserDefaults.standard.set(newValue, forKey: Constants.kUserProfile_email)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: Constants.kUserProfile_email)
        }
    }
    
    var password : String?  {
        set {
            UserDefaults.standard.set(newValue, forKey: Constants.kUserProfile_password)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: Constants.kUserProfile_password)
        }
    }
    
    var first_name : String?  {
        set {
            UserDefaults.standard.set(newValue, forKey: Constants.kUserProfile_first_name)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: Constants.kUserProfile_first_name)
        }
    }
    
    var last_name : String?  {
        set {
            UserDefaults.standard.set(newValue, forKey: Constants.kUserProfile_last_name)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: Constants.kUserProfile_last_name)
        }
    }
    
    
    var full_name : String?  {
        set {
            UserDefaults.standard.set(newValue, forKey: Constants.kUserProfile_full_name)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: Constants.kUserProfile_full_name)
        }
    }
    
    var push_token : String?  {
        set {
            UserDefaults.standard.set(newValue, forKey: Constants.kUserProfile_push_token)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: Constants.kUserProfile_push_token)
        }
    }
    
    func updateUserInfo() {
        RestAPI().getUserInfo { (isOk, info) in
            if isOk, let info = info {
                self.id = info.id
                self.username = info.username
                self.email = info.email
                self.full_name = info.full_name
            }
        }
    }
    
 }
