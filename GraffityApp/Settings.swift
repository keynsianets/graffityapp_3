//
//  Settings.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/16/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import Foundation

struct Settings {
    static let SERVER_URL = "https://af8hgqk79h.execute-api.eu-west-1.amazonaws.com/"
    static let defaultGraffitiTitle = "empty title"
    static let noTitle = "No title"
}
