//
//  AlexExtensions.swift
//  GraffityApp
//
//  Created by Alex Suvorov on 5/2/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit
import Foundation

extension UIView {
    func addConstraintsWithFormat(_ format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
    func anchor(top: NSLayoutYAxisAnchor?, left: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, right: NSLayoutXAxisAnchor?,  paddingTop: CGFloat, paddingLeft: CGFloat, paddingBottom: CGFloat, paddingRight: CGFloat, width: CGFloat, height: CGFloat) {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            self.topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }
        
        if let left = left {
            self.leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: paddingBottom).isActive = true
        }
        
        if let right = right {
            rightAnchor.constraint(equalTo: right, constant: paddingRight).isActive = true
        }
        
        if width != 0 {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        
        if height != 0 {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
}

//extension UILabel {
//    func addCharSpacing(text:String) {
//        let attrString = NSMutableAttributedString(string: text)
//        let style = NSMutableParagraphStyle()
//        style.alignment = .center
//        style.lineBreakMode = .byWordWrapping
//
//        // Line spacing attribute
//        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSRange(location: 0, length: text.count))
//
//        // Character spacing attribute
//        attrString.addAttribute(NSAttributedString.Key.kern, value: 0.4, range: NSMakeRange(0, attrString.length))
//
//        guard let font = UIFont(name: font.name_of_main_regular_font, size: 10.0) else {return}
//        attrString.addAttribute(NSAttributedString.Key.font, value: font, range: NSMakeRange(0, attrString.length))
//
//        self.attributedText = attrString
//    }
//}

extension CALayer {
    func applySketchShadow(color: UIColor, alpha: Float, x: CGFloat, y: CGFloat, blur: CGFloat, spread: CGFloat){
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
        if spread == 0 {
            shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

func isValidEmail(email: String) -> Bool {
    let RegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
    return Test.evaluate(with: email)
}

func isValidUsername(username: String) -> Bool {
    let RegEx = "^[A-Za-z0-9-\\_]+$"
    let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
    return Test.evaluate(with: username)
}

func isValidPassword(password: String) -> Bool {
    if password.count < 8 {
        return false
    }
    return true
}

extension UIViewController {
    func showAlertOk(_ withTitle:String, andText:String, action: UIAlertAction? = nil) {
        let alert = UIAlertController(title: withTitle, message: andText, preferredStyle: UIAlertController.Style.alert)
        if let action = action {
            alert.addAction(action)
        } else {
            alert.addAction(UIAlertAction(title: "OK", style: .default))
        }
        self.present(alert, animated: true, completion: nil)
    }
}



//extension UIViewController {
//    func printAlert(title: String, message: String) {
//        let alrt = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        alrt.addAction(UIAlertAction(title: StringsConstants.okAlertTitle, style: .default, handler: nil))
//        self.present(alrt, animated: true, completion: nil)
//    }
//}
