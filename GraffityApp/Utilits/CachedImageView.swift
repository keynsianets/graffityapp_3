//
//  CachedImageView.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/16/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit
import Nuke
import NukeWebPPlugin
import MapleBacon

class CachedImageView: UIImageView {
    let loadingIndicator = UIActivityIndicatorView(style: .gray)
    var task: ImageTask?
    
    func setImageForURL(stringUrl: String, placeholder: UIImage? = nil, callback: ((UIImage?, URL?) -> Void)? = nil) {
        
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.isHidden = false
        loadingIndicator.center = self.center
        loadingIndicator.startAnimating()
        self.addSubview(loadingIndicator)
        let stringImageUrl = stringUrl
        if let imageUrl = URL(string : stringImageUrl) {
            self.setImage(with: imageUrl) { (image) in
                self.loadingIndicator.stopAnimating()
            }
            // Nuke direct
            /*let options = ImageLoadingOptions(
                placeholder: UIImage(),
                failureImage: UIImage(),
                contentModes: .init(
                    success: .scaleAspectFill,
                    failure: .center,
                    placeholder: .center
                )
            )
            Nuke.loadImage(with: imageUrl, options: options, into: self, progress: { _, completed, total in }, completion: { response, error in
                self.loadingIndicator.stopAnimating()
                let image = response?.image
                callback?(image, imageUrl)
            })*/
        }
    }
}
