//
//  CheckGPSisPresent.swift
//  GraffityApp
//
//  Created by Denis Markov on 9/18/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import Foundation


class CheckGPSisPresent {
    
    static func check(dict: [String: Any]) -> Bool {
        if let gps = dict["{GPS}"] as? [String: Any], let _ = gps["Altitude"] as? Double, let _ = gps["Longitude"] as? Double, let _ = gps["Latitude"] as? Double {
            return true
        }
        return false
    }
}
