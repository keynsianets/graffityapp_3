//
//  SegmentedControlWithCorner.swift
//  GraffityApp
//
//  Created by Денис Марков on 10.02.2020.
//  Copyright © 2020 KinectPro. All rights reserved.
//

import UIKit

class CustomSegmentedControl: UIView {
        
    override func layoutSubviews() {
        self.layer.cornerRadius = self.bounds.height / 2
        self.layer.masksToBounds = true
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor(named: Colors.blueColorName)?.cgColor
    }

    
}

