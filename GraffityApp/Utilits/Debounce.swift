//
//  Debounce.swift
//  GraffityApp
//
//  Created by Denis Markov on 1/14/20.
//  Copyright © 2020 KinectPro. All rights reserved.
//

import Foundation

typealias EmptyClosure = () -> Void

final class Debouncer {
    private (set) var currentWorkItem: DispatchWorkItem?

    func debounce(delay: DispatchTimeInterval,
                  queue: DispatchQueue = .main,
                  action: @escaping EmptyClosure) -> EmptyClosure {
        return { [weak self] in
            guard let self = self else { return }

            self.currentWorkItem?.cancel()
            self.currentWorkItem = DispatchWorkItem { action() }

            queue.asyncAfter(deadline: .now() + delay, execute: self.currentWorkItem!)
        }
    }
    
    deinit {
        currentWorkItem?.cancel()
        currentWorkItem = nil
    }
}
