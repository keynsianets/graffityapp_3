//
//  ShowDropdownMessage.swift
//  ShopLook-iOS
//
//  Created by Ardlan Khalili on 7/2/18.
//  Copyright © 2018 kinect.pro. All rights reserved.
//

import UIKit


class DropdownView: NSObject {
    
    let dropdownText: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = .white
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.font = UIFont(name: "Avenir-Roman", size: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let mainView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.scarlet
        //view.layer.applySketchShadow(color: .black, alpha: 0.25, x: 0, y: 2, blur: 4, spread: 0)
        return view
    }()
    
    func showMessage(message: String, completion: (() -> Swift.Void)? = nil){
        if let window = UIApplication.shared.keyWindow {
            
            window.addSubview(mainView)
            let height: CGFloat = 88.0
            mainView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: 0)
            
            mainView.addSubview(dropdownText)
            
            dropdownText.anchor(top: mainView.topAnchor, left: mainView.leftAnchor, bottom: mainView.bottomAnchor, right: mainView.rightAnchor, paddingTop: 16, paddingLeft: 16, paddingBottom: -2, paddingRight: -4, width: 0, height: 0)
            dropdownText.text = message
            
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.25, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.mainView.frame = CGRect(x: 0, y: 0, width: self.mainView.frame.width, height: height)
                
            }, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                self.handleDismiss()
                if let completion = completion {
                    completion()
                }
            }
        }
    }
    
    func handleDismiss() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.mainView.frame = CGRect(x: 0, y: 0, width: self.mainView.frame.width, height: 0)
        } , completion: nil)
    }
    
    override init() {
        super.init()
        
    }
}
