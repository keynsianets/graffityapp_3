//
//  ErrorTextField.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/16/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import Foundation
import Material

fileprivate let paddingForIcons = UIEdgeInsets(top: 0, left: 47, bottom: 0, right: 5)

struct TextFieldSettings {
    static let font = UIFont(name: Fonts.avenirBook, size: 16)
    static let detailFont = UIFont(name: Fonts.avenirBook, size: 16)
    static let colorText = UIColor(named: Colors.textName)
}

class ErrorTextFieldWithIcons: CustomErrorTextField {
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddingForIcons)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddingForIcons)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddingForIcons)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        configTextField()

    }
}

extension CustomTextField {
    func configTextField() {
       
        let colorText = TextFieldSettings.colorText
        

        self.placeholderNormalColor = colorText ?? Colors.brownishGrey
        self.placeholderActiveColor = colorText ?? Colors.brownishGrey
        self.dividerNormalColor = Colors.placeholderColor
        self.dividerActiveColor = UIColor(named: Colors.blueColorName) ?? Colors.darkSkyBlue
        self.placeholderLabel.font = TextFieldSettings.font
        self.font = TextFieldSettings.font
        self.placeholderVerticalOffset = 25;
        self.detailLabel.font = TextFieldSettings.detailFont
        self.placeholderLabel.textColor = colorText
        self.textColor = colorText
        self.isPlaceholderUppercasedWhenEditing = false
    }
}

extension CustomErrorTextField {
    func makeError(_ error: Bool) {
        if error {
            self.dividerActiveColor = Colors.scarlet
            self.dividerColor = Colors.scarlet
            self.placeholderActiveColor = Colors.scarlet
            self.placeholderNormalColor = Colors.scarlet
        } else {
            self.dividerNormalColor = TextFieldSettings.colorText  ?? Colors.brownishGrey
            self.dividerActiveColor = UIColor(named: Colors.blueColorName) ?? Colors.darkSkyBlue
            self.placeholderNormalColor = TextFieldSettings.colorText  ?? Colors.brownishGrey
            self.placeholderActiveColor = TextFieldSettings.colorText  ?? Colors.brownishGrey
        }
    }
}
