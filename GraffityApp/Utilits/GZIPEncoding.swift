//
//  GZIPExtension.swift
//  GraffityApp
//
//  Created by Denis Markov on 12/19/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import Alamofire
import Gzip

public struct GZIPEncoding: ParameterEncoding {
    
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        
        var request = try urlRequest.asURLRequest()
        
        guard let parameters = parameters else { return request }
        
        do {
            let data = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            
            if request.value(forHTTPHeaderField: "Content-Type") == nil {
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            }
            
            request.httpBody = try data.gzipped()
            
            if request.httpBody != nil {
                request.setValue("gzip", forHTTPHeaderField: "Content-Encoding")
            }
            
        } catch {
            throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
        }
        
        return request
    }
}
