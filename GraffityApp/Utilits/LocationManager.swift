//
//  Location.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/16/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit
import CoreLocation

protocol LocationDelegate {
    func didChangeAuthorization()
}

class LocationManager: NSObject, CLLocationManagerDelegate {
    static let shared = LocationManager()
    let locationManager = CLLocationManager()
    var delegate: LocationDelegate?
    var enabled = false
    
    override init() {
        super.init()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    convenience init(delegate: LocationDelegate) {
        self.init()
        self.delegate = delegate
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if (status == CLAuthorizationStatus.denied) {
            // The user denied authorization
            delegate?.didChangeAuthorization()
        } else if (status == CLAuthorizationStatus.authorizedAlways) {
            enabled = true
            delegate?.didChangeAuthorization()
        } else if (status == CLAuthorizationStatus.authorizedWhenInUse) {
            enabled = true
            delegate?.didChangeAuthorization()
        }
       
    }
    
    
}
