//
//  InstaZoom.swift
//  InstaZoom
//
//  Created by Paul-Emmanuel on 04/01/17.
//  Copyright © 2017 rstudio. All rights reserved.
//

import UIKit

protocol PinchableImageViewDelegate {
    func pinchStarted(imageView:PinchableImageView)
}

// MARK: - UIImageView extension to easily replicate Instagram zooming feature
class PinchableImageView : CachedImageView, UIGestureRecognizerDelegate {
    /// Key for associated object
    private struct AssociatedKeys {
        static var ImagePinchKey: Int8 = 0
        static var ImagePinchGestureKey: Int8 = 1
        static var ImagePanGestureKey: Int8 = 2
        static var ImageScaleKey: Int8 = 3
    }

    public var delegate : PinchableImageViewDelegate?
    
    /// The image should zoom on Pinch
    public var isPinchable: Bool {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.ImagePinchKey) as? Bool ?? false
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.ImagePinchKey, newValue, .OBJC_ASSOCIATION_RETAIN)

            if pinchGesture == nil {
                inititialize()
            }

            if newValue {
                isUserInteractionEnabled = true
                pinchGesture.map { addGestureRecognizer($0) }
                panGesture.map { addGestureRecognizer($0) }
            } else {
                pinchGesture.map { removeGestureRecognizer($0) }
                panGesture.map { removeGestureRecognizer($0) }
            }
        }
    }

    /// Associated image's pinch gesture
    private var pinchGesture: UIPinchGestureRecognizer? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.ImagePinchGestureKey) as? UIPinchGestureRecognizer
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.ImagePinchGestureKey, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }

    /// Associated image's pan gesture
    private var panGesture: UIPanGestureRecognizer? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.ImagePanGestureKey) as? UIPanGestureRecognizer
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.ImagePanGestureKey, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }

    /// Associated image's scale -- there might be no need
    private var scale: CGFloat {
        get {
            return (objc_getAssociatedObject(self, &AssociatedKeys.ImageScaleKey) as? CGFloat) ?? 1.0
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.ImageScaleKey, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    private var previousPoint = CGPoint(x: 0, y: 0)


    /// Initialize pinch & pan gestures
    private func inititialize() {
        pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(imagePinched(_:)))
        pinchGesture?.delegate = self
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(imagePanned(_:)))
        panGesture?.delegate = self
    }

    /// Perform the pinch to zoom if needed.
    ///
    /// - Parameter sender: UIPinvhGestureRecognizer
    @objc private func imagePinched(_ pinch: UIPinchGestureRecognizer) {
        if pinch.state == .began {
            delegate?.pinchStarted(imageView: self)
        }
        scale = pinch.scale
        transform(withTranslation: .zero)

    }

    /// Perform the panning if needed
    ///
    /// - Parameter sender: UIPanGestureRecognizer
    @objc private func imagePanned(_ pan: UIPanGestureRecognizer) {
        if pan.state == .began {
            delegate?.pinchStarted(imageView: self)
        }
        let translationPoint = pan.translation(in: self)
        let newPoint = CGPoint(x: previousPoint.x + translationPoint.x, y: previousPoint.y + translationPoint.y)
        if scale > 1.0 {
            transform(withTranslation: newPoint)
        }
        if pan.state == .ended {
           previousPoint = newPoint
        }
        

    }

    /// Will transform the image with the appropriate
    /// scale or translation.
    ///
    /// Parameter translation: CGPoint
    private func transform(withTranslation translation: CGPoint) {
        var transform = CATransform3DIdentity
        transform = CATransform3DScale(transform, scale, scale, 1.01)
        transform = CATransform3DTranslate(transform, translation.x, translation.y, 0)
        layer.transform = transform
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

