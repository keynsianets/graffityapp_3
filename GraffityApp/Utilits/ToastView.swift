//
//  ToastView.swift
//  GraffityApp
//
//  Created by Alex Suvorov on 9/4/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit

class ToastView: UIView {
    let label = UILabel()
    
    override func awakeFromNib() {
        label.textColor = .white
        label.font = UIFont(name: Fonts.avenirBlack, size: 20)
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        let leadingConstraint = NSLayoutConstraint(item: label, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute:NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
        let trailingConstraint = NSLayoutConstraint(item: label, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute:NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
        let verticalConstraint = NSLayoutConstraint(item: label, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        
        NSLayoutConstraint.activate([leadingConstraint, trailingConstraint, verticalConstraint])
        
        self.layoutIfNeeded()
    }
    
    func showToast(text:String, duration: Double) {
        label.text = text
        label.textAlignment = .center
        self.isHidden = false
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1
        }) { (isEnd) in
            if isEnd {
                UIView.animate(withDuration: duration, animations: {
                    self.alpha = 0
                }, completion: { (isEnd) in
                    if isEnd {
                        self.isHidden = true
                    }
                })
            }
        }
        
    }
}
