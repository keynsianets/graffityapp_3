//
//  UIView+slideAnimating.swift
//  Xeroe
//
//  Created by Денис Марков on 9/24/19.
//  Copyright © 2019 Денис Марков. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    public enum Animation {
        case down
        case up
        case none
    }
    
    func slideIn(from edge: Animation = .none, x: CGFloat = 0, y: CGFloat = 0, durations: TimeInterval = 0.5, delay: TimeInterval = 0, completion: ((Bool) -> Void)? = nil) {
          
        UIView.animate(withDuration: durations, delay: delay, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
            self.transform = CGAffineTransform(translationX: self.frame.width, y: 0)
            }, completion: completion)

    }
    
//    func slideOut(from edge: Animation = .none, x: CGFloat = 0, y: CGFloat = 0, durations: TimeIntself.erval = 1, delay: TimeInterval = 1, completion: ((Bool) -> Void)? = nil) {
//
//        let offset = CGPoint(x: frame.origin.x, y: 0)
//        let endtransform = CGAffineTransform(translationX: offset.x - frame.width, y: offset.y + y)
//
//        UIView.animate(withDuration: durations, delay: delay, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
//            self.transform = endtransform
//            }, completion: completion)
//
//    }
    
    private func offcetFor(edge: Animation) -> CGPoint {
        if let size = self.superview?.frame.size {
            switch edge {
            case .none :
                return .zero
            case .down:
                return CGPoint(x: 0, y: -frame.maxY)
            case .up:
                return CGPoint(x: 0, y: size.height - frame.minY)
            }
        }
        return .zero
    }
}
