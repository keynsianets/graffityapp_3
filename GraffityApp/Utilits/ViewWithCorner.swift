//
//  ViewWithCorner.swift
//  GraffityApp
//
//  Created by Денис Марков on 10.02.2020.
//  Copyright © 2020 KinectPro. All rights reserved.
//

import UIKit

class ViewWithCorner: UIView {

    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 8
        self.layer.masksToBounds = true

    }

}
