//
//  AddFirstSnapeViewController.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/15/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit

class AddFirstSnapeViewController: UIViewController {
    
    @IBOutlet var mainBackgroundView: UIView! {
        didSet {
            mainBackgroundView.backgroundColor = UIColor(named: Colors.backgroundName)
        }
    }
    
    @IBOutlet weak var screenTitleLabel: UILabel! {
        didSet {
            screenTitleLabel.text = Strings.addFirstSnap
            screenTitleLabel.font = UIFont(name: Fonts.avenirBlack, size: 28)
            screenTitleLabel.textColor = UIColor(named: Colors.textName)

        }
    }
    
    @IBOutlet weak var screenDescriptionLabel: UILabel!  {
        didSet {
            screenDescriptionLabel.text = Strings.pleaseShareYourFirstSnap
            screenDescriptionLabel.font = UIFont(name: Fonts.avenirRoman, size: 18)
            screenDescriptionLabel.textColor = UIColor(named: Colors.textName)

        }
    }
    
    @IBOutlet weak var takePhotoButton: UIButton! {
        didSet {
            takePhotoButton.setTitle(Strings.takePhoto, for: .normal)
            takePhotoButton.titleLabel?.font = UIFont(name: Fonts.avenirRoman, size: 16)
            takePhotoButton.setTitleColor(Colors.white, for: .normal)
            takePhotoButton.addTarget(self, action: #selector(goNext), for: .touchUpInside)
            takePhotoButton.backgroundColor = UIColor(named: Colors.blueColorName)

        }
    }
    
    @IBOutlet weak var photoIcon: UIImageView!
    @IBOutlet weak var listPushIcon: UIImageView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @objc func goNext() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let addFirstSnapeVC = storyboard.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
        addFirstSnapeVC.isFirstOpen = true
        self.navigationController?.pushViewController(addFirstSnapeVC, animated: true)
    }
    
    
    
}
