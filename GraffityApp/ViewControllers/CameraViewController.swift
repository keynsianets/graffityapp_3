//
//  CameraViewController.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/16/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit
import CameraManager
import Photos
//import ImageIO

class CameraViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet var mainBackgroundView: UIView! {
        didSet {
            mainBackgroundView.backgroundColor = UIColor(named: Colors.backgroundName)
        }
    }
    
    @IBOutlet weak var tabBarView: UIView! {
        didSet {
            tabBarView.backgroundColor = UIColor(named: Colors.tabBarName)
        }
    }
    
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            imageView.isUserInteractionEnabled = true
        }
    }
    
    @IBOutlet weak var closeButton: UIButton! {
        didSet {
            closeButton.addTarget(self, action: #selector(closeButtonDidTap), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var photoButtonBorderView: UIView! {
        didSet {
            photoButtonBorderView.layer.cornerRadius = photoButtonBorderView.frame.height / 2
            photoButtonBorderView.layer.masksToBounds = true
            photoButtonBorderView.layer.borderColor = UIColor(rgb: 0xd9d9d9).cgColor
            photoButtonBorderView.layer.borderWidth = 1
            photoButtonBorderView.backgroundColor = Colors.scarlet
        }
    }
    
    @IBOutlet weak var galleryButton: UIButton! {
        didSet {
            galleryButton.addTarget(self, action: #selector(galleryButtonDidTap), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var photoButton: UIButton! {
        didSet {
            photoButton.layer.cornerRadius = photoButton.frame.height / 2
            photoButton.layer.masksToBounds = true
            photoButton.addTarget(self, action: #selector(photoButtonDidTap), for: .touchUpInside)
        }
    }
    
    let session = AVCaptureSession()
    
    let cameraManager = CameraManager()
    
    var image: UIImage?
    var metadata : [String : Any]?
    var imagePicker = UIImagePickerController()
    var source = "LIVE"
    var isFirstOpen = false
    var needUseSecondary = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cameraManager.addPreviewLayerToView(self.imageView)
        cameraManager.shouldEnableExposure = true
        cameraManager.shouldUseLocationServices = true
        cameraManager.writeFilesToPhoneLibrary = true
        cameraManager.imageAlbumName =  "com.graffititagr"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setLayoutForPhotoButton()
        setLayoutForPhotoButtonBorderView()
    }
    
    func setLayoutForPhotoButtonBorderView() {
        photoButtonBorderView.layer.cornerRadius = photoButtonBorderView.frame.height / 2
        photoButtonBorderView.layer.masksToBounds = true
        photoButtonBorderView.layer.borderColor = UIColor(rgb: 0xd9d9d9).cgColor
        photoButtonBorderView.layer.borderWidth = 1
    }
    
    func setLayoutForPhotoButton() {
        photoButton.layer.cornerRadius = photoButton.frame.height / 2
        photoButton.layer.masksToBounds = true
    }
    
    @objc func photoButtonDidTap() {
        
        cameraManager.capturePictureWithCompletion { (result) in
            switch result {
            case .failure(let error) :
                print("error", error.localizedDescription)
            case.success(let content) :
                switch content {
                case .asset(let asset) :
                    debugPrint("capturePictureWithCompletion asset")
                    let options = PHImageRequestOptions()
                    options.isNetworkAccessAllowed = true
                    options.isSynchronous = true
                    options.version = PHImageRequestOptionsVersion.original
                    
                    let manager = PHImageManager()
                    manager.requestImageData(for: asset, options: options) { (imageData, dataUTI, orientation, info) in
                        DispatchQueue.main.async {
                            self.needUseSecondary = false
                            if let data = imageData, let ciImage = CIImage(data: data) {
                                self.metadata = ciImage.properties
                            }
                            self.source = "LIVE"
                            self.image = content.asImage
                            self.goNext()
                        }
                        
                    }
                    
                case .image(let image):
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                        if self.needUseSecondary {
                             debugPrint("capturePictureWithCompletion image")
                            if let data = image.getExifData() as? [String: AnyObject] {
                                self.metadata = data
                                print(data)
                            }
                            self.source = "LIVE"
                            self.image = image
                            self.goNext()
                        }
                    })
                    break
                    
                case .imageData(let data):
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                        if self.needUseSecondary {
                            debugPrint("capturePictureWithCompletion data")
                            if let ciImage = CIImage(data: data) {
                                self.metadata = ciImage.properties
                                print(ciImage.properties)
                            }
                            self.source = "LIVE"
                            self.image = content.asImage
                            self.goNext()
                        }
                    })
                    break
                }
                
            }
            
        }
        
    }
    
    @objc func galleryButtonDidTap() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let chosenImage = info[.originalImage] as? UIImage{
            debugPrint("imagePickerController did choose image")
            picker.dismiss(animated: true, completion: nil)
            self.image = chosenImage
            self.imageView.image = chosenImage
            self.source = "CAMERA_ROLL"
            let action = UIAlertAction(title: Strings.ok.uppercased(), style: .default, handler: { (action) in
                self.goNext()
            })
            let fileURL = info[.imageURL]
            if let imageSource = CGImageSourceCreateWithURL(fileURL as! CFURL, nil) {
                let imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil)
                if let dict = imageProperties as? [String: Any] {
                    print(dict)
                    self.metadata = dict
                    if !CheckGPSisPresent.check(dict: dict) {
                        DispatchQueue.main.async {
                            self.showAlertOk(Strings.cantGetGPSData, andText: Strings.checkYouAllowCameraGetLocation, action: action)
                        }
                        
                    } else {
                        self.goNext()
                    }
                } else {
                    self.showAlertOk(Strings.cantGetGPSData, andText: Strings.cantGetEXIF, action: action)
                }
            } else {
                self.showAlertOk(Strings.cantGetGPSData, andText: Strings.cantGetEXIF, action: action)
            }
            return
        }
        
    }
    
    
    @objc func goNext() {
        guard let image = image else { return }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let addFirstSnapeVC = storyboard.instantiateViewController(withIdentifier: "UploadPhotoViewController") as! UploadPhotoViewController
        addFirstSnapeVC.source = source
        addFirstSnapeVC.image = image
        addFirstSnapeVC.metadata = metadata
        self.navigationController?.pushViewController(addFirstSnapeVC, animated: true)
    }
    
    @objc func closeButtonDidTap() {
        if isFirstOpen {
            let tabBar = MainTabBarViewController()
            self.navigationController?.pushViewController(tabBar, animated: true)
        } else {
            backButtonDidTap()
        }
    }
    
    @objc func backButtonDidTap() {
        let vcs = self.navigationController?.viewControllers
        if ((vcs?.contains(self)) != nil) {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
