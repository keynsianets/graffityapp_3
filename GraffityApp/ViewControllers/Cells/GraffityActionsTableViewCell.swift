//
//  GraffityActionsTableViewCell.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/16/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit

protocol GraffityActionsTableViewCellDelegate {
    func editButtonDidTap(graffity: Image, cell: GraffityActionsTableViewCell)
    func deleteButtonDidTap(graffity: Image, cell: GraffityActionsTableViewCell)
    func solvedButtonDidTap(graffity: Image, cell: GraffityActionsTableViewCell)
}

class GraffityActionsTableViewCell: UITableViewCell {

    @IBOutlet weak var graffityDescriptionLabel: UILabel! {
        didSet {
            graffityDescriptionLabel.font = UIFont(name: Fonts.avenirLight, size: 16)
            graffityDescriptionLabel.textColor = UIColor(named: Colors.textName)
        }
    }
    
    @IBOutlet weak var graffityAdressLabel: UILabel! {
        didSet {
            graffityAdressLabel.font = UIFont(name: Fonts.robotoLight, size: 12)
            graffityAdressLabel.textColor = UIColor(named: Colors.textName)
        }
    }
    
    @IBOutlet weak var solvedButton: UIButton! {
        didSet {
            solvedButton.setTitle(Strings.solved, for: .normal)
            solvedButton.titleLabel?.font = UIFont(name: Fonts.avenirLight, size: 12)
            solvedButton.addTarget(self, action: #selector(solvedButtonDidTap), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var editButton: UIButton! {
        didSet {
            editButton.setTitle(Strings.edit, for: .normal)
            editButton.titleLabel?.font = UIFont(name: Fonts.avenirRoman, size: 16)
            editButton.addTarget(self, action: #selector(editButtonDidTap), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var deleteButton: UIButton! {
        didSet {
            deleteButton.setTitle(Strings.delete, for: .normal)
            deleteButton.titleLabel?.font = UIFont(name: Fonts.avenirRoman, size: 16)
            deleteButton.addTarget(self, action: #selector(deleteButtonDidTap), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var locationPinIcon: UIImageView!
    
    var graffity: Image?
    
    var delegate: GraffityActionsTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUp(graffity: Image) {
        graffityDescriptionLabel.text = graffity.description
        graffityAdressLabel.text = graffity.address
        self.graffity = graffity
    }
    
    @objc func solvedButtonDidTap() {
        guard let graffity = graffity else { return }
        delegate?.solvedButtonDidTap(graffity: graffity, cell: self)
    }
    
    @objc func editButtonDidTap() {
        guard let graffity = graffity else { return }
        delegate?.editButtonDidTap(graffity: graffity, cell: self)
    }
    
    @objc func deleteButtonDidTap() {
        guard let graffity = graffity else { return }
        delegate?.deleteButtonDidTap(graffity: graffity, cell: self)
    }
    
}
