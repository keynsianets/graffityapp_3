//
//  GraffityCollectionViewCell.swift
//  GraffityApp
//
//  Created by Денис Марков on 10.02.2020.
//  Copyright © 2020 KinectPro. All rights reserved.
//

import UIKit

class GraffityCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var contenBackgroundView: ViewWithCorner! {
        didSet {
            contenBackgroundView.backgroundColor = UIColor(named: Colors.cellBackgroundName)
        }
    }
    
    
    @IBOutlet weak var graffityImageView: CachedImageView! {
        didSet {
            graffityImageView.layer.masksToBounds = true
            graffityImageView.layer.cornerRadius = 8
        }
    }
    
    @IBOutlet weak var graffityAdressLabel: UILabel! {
        didSet {
            graffityAdressLabel.font = UIFont(name: Fonts.robotoLight, size: 12)
            graffityAdressLabel.textColor = UIColor(named: Colors.textName)
        }
    }
    
    @IBOutlet weak var graffityStatusLabel: UILabel!  {
        didSet {
            graffityStatusLabel.font = UIFont(name: Fonts.avenirLight, size: 12)
            graffityStatusLabel.textColor = UIColor(named: Colors.textName)
        }
    }
    
    @IBOutlet weak var locationPinIcon: UIImageView!
    
    
    @IBOutlet weak var widthContetnBackgroundView: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        let path = UIBezierPath(roundedRect: contenBackgroundView.bounds, byRoundingCorners:[.allCorners], cornerRadii: CGSize(width: 8, height: 8))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        contenBackgroundView.layer.mask = maskLayer
    }
    
    
    //        override func setSelected(_ selected: Bool, animated: Bool) {
    //            super.setSelected(selected, animated: animated)
    //
    //            // Configure the view for the selected state
    //        }
    
    func setUp(graffity: Image, width: CGFloat) {
        if let imageUrl = graffity.image {
            graffityImageView.setImageForURL(stringUrl: imageUrl)
        }
        //graffityTitleLabel.text = graffity.title
        graffityAdressLabel.text = graffity.address
        graffityStatusLabel.text = graffity.status
        widthContetnBackgroundView.constant = width
    }
    
    override func prepareForReuse() {
        graffityImageView.image = nil
        
        // graffityTitleLabel.text = nil
        graffityAdressLabel.text = nil
        graffityStatusLabel.text = nil
    }
    
}
