//
//  GraffityTableViewCell.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/16/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit

class GraffityTableViewCell: UITableViewCell {
    

    @IBOutlet weak var contenBackgroundView: UIView! {
        didSet {
            contenBackgroundView.backgroundColor = UIColor(named: Colors.cellBackgroundName)
        }
    }
    
    @IBOutlet weak var graffityAdressLableTop: NSLayoutConstraint!
    
    
    @IBOutlet weak var graffityImageView: CachedImageView! {
        didSet {
            graffityImageView.layer.masksToBounds = true
            graffityImageView.layer.cornerRadius = 8
        }
    }
    
    @IBOutlet weak var graffityTitleLabel: UILabel! {
        didSet {
            graffityTitleLabel.font = UIFont(name: Fonts.avenirLight, size: 16)
            graffityTitleLabel.textColor = UIColor(named: Colors.textName)
        }
    }
    
    @IBOutlet weak var graffityAdressLabel: UILabel!  {
        didSet {
            graffityAdressLabel.font = UIFont(name: Fonts.robotoLight, size: 12)
            graffityAdressLabel.textColor = UIColor(named: Colors.textName)
}
    }
    
    @IBOutlet weak var graffityStatusLabel: UILabel!  {
        didSet {
            graffityStatusLabel.font = UIFont(name: Fonts.avenirLight, size: 12)
            graffityStatusLabel.textColor = UIColor(named: Colors.textName)
}
    }
    
    @IBOutlet weak var locationPinIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        let path = UIBezierPath(roundedRect:bounds, byRoundingCorners:[.allCorners], cornerRadii: CGSize(width: 8, height: 8))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        layer.mask = maskLayer
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setUp(graffity: Image) {
        if let imageUrl = graffity.image {
            graffityImageView.setImageForURL(stringUrl: imageUrl)
        }
        //graffityTitleLabel.text = graffity.title
        graffityAdressLabel.text = graffity.address
        graffityStatusLabel.text = graffity.status
        graffityAdressLableTop.constant = graffityTitleLabel.frame.size.height > 1 ? 10 : 0
       
    }
    
    override func prepareForReuse() {
        graffityImageView.image = nil
        
        // graffityTitleLabel.text = nil
        graffityAdressLabel.text = nil
        graffityStatusLabel.text = nil
    }
    
}
