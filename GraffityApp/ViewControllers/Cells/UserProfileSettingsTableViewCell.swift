//
//  UserProfileSettingsTableViewCell.swift
//  GraffityApp
//
//  Created by Alex Suvorov on 9/4/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit

class UserProfileSettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var cellBackgroundView: UIView!
    @IBOutlet weak var leftIcon: UIImageView!
    @IBOutlet weak var label: UILabel! {
        didSet {
            label.font = UIFont(name: Fonts.avenirBook, size: 16)
            label.textColor = UIColor(named: Colors.textName)
        }
    }
    @IBOutlet weak var rightIcon: UIImageView!
    @IBOutlet weak var separatorView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: UserSettingsCellData) {
        label.text = data.title
        leftIcon.image = data.image
    }

}
