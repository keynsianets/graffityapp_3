//
//  EditGraffityViewController.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/16/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit

class EditGraffityViewController: UIViewController {
    
    @IBOutlet var mainBackgroundView: UIView! {
        didSet {
            mainBackgroundView.backgroundColor = UIColor(named: Colors.backgroundName)
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.font = UIFont(name: Fonts.avenirMedium, size: 20)
            titleLabel.textColor = UIColor(named: Colors.textName)
        }
    }
    
    @IBOutlet weak var imageView: CachedImageView! {
        didSet {
            imageView.layer.masksToBounds = true
            imageView.layer.cornerRadius = 5
            imageView.contentMode = .scaleAspectFill
        }
    }
    
    @IBOutlet weak var descriptionLabel: UILabel!  {
        didSet {
            descriptionLabel.font = UIFont(name: Fonts.avenirMedium, size: 18)
            descriptionLabel.textColor = UIColor(named: Colors.textName)

        }
    }
    
    @IBOutlet weak var addressLabel: UILabel!   {
        didSet {
            addressLabel.font = UIFont(name: Fonts.avenirMedium, size: 18)
            addressLabel.textColor = UIColor(named: Colors.textName)

        }
    }
    
    @IBOutlet weak var backButton: UIButton! {
        didSet {
            backButton.setTitle(Strings.back.uppercased(), for: .normal)
            backButton.titleLabel?.font = UIFont(name: Fonts.avenirRoman, size: 16)
            backButton.backgroundColor = UIColor(named: Colors.backgroundName)
            backButton.setTitleColor(UIColor(named: Colors.textName), for: .normal)
            backButton.addTarget(self, action: #selector(backButtonDidTap), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var deleteButton: UIButton! {
        didSet {
            deleteButton.setTitle(Strings.delete.uppercased(), for: .normal)
            deleteButton.backgroundColor = Colors.scarlet
            deleteButton.setTitleColor(Colors.white, for: .normal)
            deleteButton.titleLabel?.font = UIFont(name: Fonts.avenirRoman, size: 16)
        }
    }
    
    @IBOutlet weak var locationPinIcon: UIImageView!
    
    var graffity: Image?    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let imageUrl = graffity?.image {
            imageView.setImageForURL(stringUrl: imageUrl)
        }
        titleLabel.text = graffity?.description
        descriptionLabel.text = graffity?.title
        addressLabel.text = graffity?.address
        self.navigationController?.navigationBar.barStyle = .black
        
        // Do any additional setup after loading the view.
    }
    
    @objc func backButtonDidTap() {
        let vcs = self.navigationController?.viewControllers
        if ((vcs?.contains(self)) != nil) {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
