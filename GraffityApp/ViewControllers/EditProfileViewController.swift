//
//  EditProfileViewController.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/17/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {
    
    @IBOutlet var mainBackgroundView: UIView! {
        didSet {
            mainBackgroundView.backgroundColor = UIColor(named: Colors.backgroundName)
        }
    }
    
    @IBOutlet weak var backButton: UIButton! {
        didSet {
            backButton.addTarget(self, action: #selector(backButtonDidTap), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var editUserTitleLabel: UILabel! {
        didSet {
            editUserTitleLabel.text = Strings.editUser
            editUserTitleLabel.font = UIFont(name: Fonts.avenirLight, size: 36)
            editUserTitleLabel.textColor = UIColor(named: Colors.blueColorName)
        }
    }
    
    @IBOutlet weak var userNameTextField: ErrorTextFieldWithIcons! {
        didSet {
            userNameTextField.placeholder = Strings.userName
        }
    }
    
    @IBOutlet weak var emailTextField: ErrorTextFieldWithIcons! {
        didSet {
            emailTextField.placeholder = Strings.email
        }
    }
    
    @IBOutlet weak var passwordTextField: ErrorTextFieldWithIcons!  {
        didSet {
            passwordTextField.placeholder = Strings.password
        }
    }
    
    @IBOutlet weak var repeatPasswordTextField: ErrorTextFieldWithIcons!   {
        didSet {
            repeatPasswordTextField.placeholder = Strings.password
        }
    }
    
    @IBOutlet weak var updateButton: UIButton! {
        didSet {
            updateButton.setTitle(Strings.update, for: .normal)
            updateButton.titleLabel?.font = UIFont(name: Fonts.avenirRoman, size: 16)
            updateButton.backgroundColor = UIColor(named: Colors.blueColorName)
            updateButton.setTitleColor(Colors.white, for: .normal)
            updateButton.layer.cornerRadius = 4
            updateButton.layer.masksToBounds = true
            updateButton.addTarget(self, action: #selector(updateButtonDidTap), for: .touchUpInside)
        }
    }
    @IBOutlet weak var iconUser: UIImageView!
    @IBOutlet weak var lockIcon: UIImageView!
    @IBOutlet weak var mailIcon: UIImageView!
    @IBOutlet weak var firstLockIcon: UIImageView!
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barStyle = .black
        
        userNameTextField.text = UserProfile.shared.username
        emailTextField.text = UserProfile.shared.email
        passwordTextField.text = UserProfile.shared.password
        repeatPasswordTextField.text = UserProfile.shared.password
        
        // Do any additional setup after loading the view.
    }
    
    @objc func updateButtonDidTap() {
        //        let password = passwordTextField.text == repeatPasswordTextField.text ? passwordTextField.text : nil
        //        RestAPI().editUser(userName: userNameTextField.text, email: emailTextField.text, password: password) { (isOk, error) in
        //            if isOk {
        //                self.showAlertOk("Update successfully", andText: "")
        //                self.userNameTextField.text = UserProfile.shared.username
        //                self.emailTextField.text = UserProfile.shared.email
        //                self.passwordTextField.text = UserProfile.shared.password
        //                self.repeatPasswordTextField.text = UserProfile.shared.password
        //            }
        //        }
        self.showAlertOk("Temporary unavailable", andText: "Will work soon")
    }
    
    @objc func backButtonDidTap() {
        let vcs = self.navigationController?.viewControllers
        if ((vcs?.contains(self)) != nil) {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
