//
//  FacebookSignUpViewController.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/17/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit

class FacebookSignUpViewController: UIViewController {
    lazy var duplcateView: DropdownView = {
        let view = DropdownView()
        return view
    }()
    
    @IBOutlet var mainBackgroundView: UIView! {
        didSet {
            mainBackgroundView.backgroundColor = UIColor(named: Colors.backgroundName)
        }
    }
    
    @IBOutlet weak var backButton: UIButton! {
        didSet {
            backButton.addTarget(self, action: #selector(backButtonDidTap), for: .touchUpInside)
        }
    }
    @IBOutlet weak var newAccountLabel: UILabel!  {
           didSet {
               newAccountLabel.text = Strings.newAccount + ","
               newAccountLabel.font = UIFont(name: Fonts.avenirLight, size: 24)
            newAccountLabel.textColor = UIColor(named: Colors.textName)
           }
       }
    
    @IBOutlet weak var completeRegistrationLabel: UILabel! {
           didSet {
               completeRegistrationLabel.text = Strings.completeRegistration
               completeRegistrationLabel.font = UIFont(name: Fonts.avenirLight, size: 18)
            completeRegistrationLabel.textColor = UIColor(named: Colors.textName)
           }
       }
    
    @IBOutlet weak var userNameTextField: ErrorTextFieldWithIcons! {
        didSet {
            userNameTextField.placeholder = Strings.userName
            userNameTextField.placeholderLabel.font = UIFont(name: Fonts.avenirBook, size: 16)
            userNameTextField.font = UIFont(name: Fonts.avenirBook, size: 16)
        }
    }
    
    @IBOutlet weak var emailTextField: ErrorTextFieldWithIcons!   {
          didSet {
              emailTextField.placeholder = Strings.email
              emailTextField.placeholderLabel.font = UIFont(name: Fonts.avenirBook, size: 16)
              emailTextField.font = UIFont(name: Fonts.avenirBook, size: 16)
          }
      }
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var registerButton: UIButton! {
        didSet {
            registerButton.layer.cornerRadius = 4
            registerButton.layer.masksToBounds = true
            registerButton.setTitle(Strings.continueLabel, for: .normal)
            registerButton.titleLabel?.font = UIFont(name: Fonts.avenirRoman, size: 16)
            registerButton.titleLabel?.textColor = Colors.white
            registerButton.addTarget(self, action: #selector(registerButtonDidTap), for: .touchUpInside)
            registerButton.backgroundColor = UIColor(named: Colors.blueColorName)
        }
    }
    
    @IBOutlet weak var iAlreadyHaveAccountLabel: UILabel! {
        didSet {
            let attributedString = NSMutableAttributedString(string: Strings.iAlreadyHaveAccount, attributes: [
                .font: UIFont(name: Fonts.avenirLight, size: 14.0)!,
                .foregroundColor: UIColor(named: Colors.textName) ?? Colors.brownishGrey,
                .kern: 0.39
                ])
            attributedString.addAttribute(.foregroundColor, value: UIColor(named: Colors.blueColorName), range: NSRange(location: 27, length: 5))
            iAlreadyHaveAccountLabel.attributedText = attributedString
            iAlreadyHaveAccountLabel.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(backButtonDidTap))
            iAlreadyHaveAccountLabel.addGestureRecognizer(tap)
        }
    }
    
    @IBOutlet weak var loginButton: UIButton! {
        didSet {
            loginButton.addTarget(self, action: #selector(backButtonDidTap), for: .touchUpInside)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
         
        emailTextField.text = UserProfile.shared.email
        userNameTextField.text = UserProfile.shared.username
        // Do any additional setup after loading the view.
    }
    
    
    
    @objc func registerButtonDidTap() {
        debugPrint("register")
        
        view.endEditing(true)
       
        guard let email = emailTextField.text else {
            return
        }
        
        guard let username = userNameTextField.text else  {
            return
        }
        
    
        if username == "" {  duplcateView.showMessage(message: Strings.signUpFailed + "\n" + Strings.usernameCannotBeEmpty, completion: nil); return}
      
        if email == "" {  duplcateView.showMessage(message: Strings.signUpFailed + "\n" + Strings.emailCannotBeEmpty, completion: nil); return}
    
        guard let fb_token = UserProfile.shared.fb_token, let full_name = UserProfile.shared.full_name else {
            self.showAlertOk(Strings.error, andText: Strings.somethingWentWrong)
            
            return
        }
        
        
        self.activityIndicator.startAnimating()
        RestAPI().register(userName: username, email: email, fb_access_token: fb_token, full_name: full_name) { (isOk, error) in
            if isOk {
                
                UserProfile.shared.username = username
                UserProfile.shared.email = email
                
                RestAPI().signIn(facebookToken: fb_token, callback: { (isOk, token) in
                    self.activityIndicator.stopAnimating()
                    if let token = token {
                        
                        UserProfile.shared.token = token
                        self.gotoWelcomeVC()
                        
                    } else {
                        self.showAlertOk(Strings.error, andText: Strings.somethingWentWrong)
                    }
                })
                
                
            } else {
                if let error = error {
                    //duplicate username or email
                    self.showDuplicateError(error: error.getCommonError())
                }  else {
                    self.showAlertOk(Strings.error, andText: Strings.somethingWentWrong)
                }
            }
            
        }
        
        
        
    }
    
    
    @objc func backButtonDidTap() {
        let vcs = self.navigationController?.viewControllers
        if ((vcs?.contains(self)) != nil) {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }

    func showDuplicateError(error: String){
        duplcateView.showMessage(message:error, completion: nil)
    }
    
    func gotoWelcomeVC() {
        if !UserProfile.shared.notNeedToShowWelcomeScreens  {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let welcomeController = storyboard.instantiateViewController(withIdentifier: "WelcomeViewController") as? WelcomeViewController else {
                debugPrint("Fail")
                return
            }
            self.navigationController?.pushViewController(welcomeController, animated: true)
        } else {
            let tabBar = MainTabBarViewController()
            self.navigationController?.pushViewController(tabBar, animated: true)
        }
        
    }
}
