//
//  FullImageViewController.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/25/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit

class FullImageViewController: UIViewController {
    
    @IBOutlet var mainBackgroundView: UIView! {
        didSet {
            mainBackgroundView.backgroundColor = UIColor(named: Colors.backgroundName)
        }
    }
    @IBOutlet weak var tapBarView: UIView! {
        didSet {
            tapBarView.backgroundColor = UIColor(named: Colors.backgroundName)
        }
    }
    
    @IBOutlet weak var backButton: UIButton! {
        didSet {
            backButton.setTitle(Strings.backWithArrow, for: .normal)
            backButton.titleLabel?.font = UIFont(name: Fonts.avenirBook, size: 17)
            backButton.setTitleColor(UIColor(named: Colors.textName), for: .normal)
            backButton.addTarget(self, action: #selector(backButtonDidTap), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var imageView: PinchableImageView! {
        didSet {
            imageView.isUserInteractionEnabled = true
            imageView.isPinchable = true
            debugPrint(imageView.isPinchable, "image is pinchable")
        }
    }
    
    @IBOutlet weak var backGroundView: UIView!
    
    var graffity: Image?
    
    override func viewDidLoad() {
        super.viewDidLoad()
         
        if let imageUrl = graffity?.image {
            imageView.setImageForURL(stringUrl: imageUrl)
        }
        // Do any additional setup after loading the view.
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        UIDevice.current.setValue(Int(UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
//        
//    }
//    
//    @objc func canRotate() {}
    
    @objc func backButtonDidTap() {
        let vcs = self.navigationController?.viewControllers
        if ((vcs?.contains(self)) != nil) {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

}
