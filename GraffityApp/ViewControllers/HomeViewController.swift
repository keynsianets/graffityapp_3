//
//  HomeViewController.swift
//  GraffityApp
//
//  Created by Denis Markov on 9/20/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var segmentedController: UISegmentedControl!
    
    @IBOutlet var mainBackgroundView: UIView! {
        didSet {
            mainBackgroundView.backgroundColor = UIColor(named: Colors.tabBarBackgroundName)
        }
    }
    
    @IBOutlet weak var mapButton: UIButton! {
        didSet {
            mapButton.setTitle(Strings.map, for: .normal)
            mapButton.setTitleColor(Colors.white, for: .normal)
            mapButton.titleLabel?.font = UIFont(name: Fonts.avenirHeavy, size: 14)
            mapButton.backgroundColor = UIColor(named: Colors.blueColorName)
            mapButton.addTarget(self, action: #selector(mapButtonDidTap), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var listButton: UIButton!  {
        didSet {
            listButton.setTitle(Strings.list, for: .normal)
            listButton.setTitleColor(UIColor(named: Colors.blueColorName), for: .normal)
            listButton.titleLabel?.font = UIFont(name: Fonts.avenirHeavy, size: 14)
            listButton.backgroundColor = UIColor(named: Colors.backgroundSegmentName)
            listButton.addTarget(self, action: #selector(listButtonDidTap), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var contentView: UIView!
    var currentChildVC: UIViewController?
    let listViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ListViewController") as! ListViewController
    let mapViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentChildVC = mapViewController
        setChildController()
        // Do any additional setup after loading the view.
        
        
    }
    
    @objc func mapButtonDidTap() {
        clearContainerView()
        listButton.setTitleColor(UIColor(named: Colors.blueColorName), for: .normal)
        listButton.backgroundColor = UIColor(named: Colors.backgroundSegmentName)
        mapButton.setTitleColor(Colors.white, for: .normal)
        mapButton.backgroundColor = UIColor(named: Colors.blueColorName)
        currentChildVC = mapViewController
        setChildController()
    }
    
    @objc func listButtonDidTap() {
        clearContainerView()
        mapButton.setTitleColor(UIColor(named: Colors.blueColorName), for: .normal)
        mapButton.backgroundColor = UIColor(named: Colors.backgroundSegmentName)
        listButton.setTitleColor(Colors.white, for: .normal)
        listButton.backgroundColor = UIColor(named: Colors.blueColorName)
        
        currentChildVC = listViewController
        setChildController()
    }
    
    //        let translationX = segmentViewController.selectedSegmentIndex == 1 ? self.viewForMap.frame.width : 0
    //        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
    //            self.viewForMap.transform = CGAffineTransform(translationX: translationX, y: 0)
    //            }, completion: nil)
    
    
    func setChildController() {
        guard let child = currentChildVC else { return }
        self.addChild(child)
        child.view.frame = CGRect(x: 0, y: 0, width: self.contentView.frame.size.width, height: self.contentView.frame.size.height)
        self.contentView.addSubview(child.view)
        child.didMove(toParent: self)
        child.view.backgroundColor = UIColor(named: Colors.tabBarBackgroundName)
        currentChildVC = child
    }
    
    func clearContainerView() {
        guard let child = currentChildVC else { return }
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }
    
}
