//
//  ListViewController.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/16/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit

fileprivate let gtaffityCellReuseIdentifier = "GraffityTableViewCell"
fileprivate let gtaffityActionsCellReuseIdentifier = "GraffityActionsTableViewCell"

class ListViewController: UIViewController {
    @IBOutlet var mainBackgroundView: UIView! {
        didSet {
            mainBackgroundView.backgroundColor = UIColor(named: Colors.backgroundName)
        }
    }

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var deleteToast: UIView! {
        didSet {
            deleteToast.backgroundColor = Colors.scarlet
        }
    }
    @IBOutlet weak var yourPostHasBeenDeletedLabel: UILabel!{
        didSet {
            yourPostHasBeenDeletedLabel.text = Strings.yourPostHasBeenDeleted
            yourPostHasBeenDeletedLabel.font = UIFont(name: Fonts.avenirRoman, size: 14)
            yourPostHasBeenDeletedLabel.textColor = Colors.white
        }
    }
    
    @IBOutlet weak var bottomShadowView: UIView! {
        didSet {
            bottomShadowView.layer.shadowOffset = CGSize(width: 0, height: -3)
            bottomShadowView.layer.shadowOpacity = 0.15
            bottomShadowView.layer.shadowRadius = 15.0
            bottomShadowView.layer.shadowColor = UIColor.black.cgColor
            bottomShadowView.layer.masksToBounds = false
        }
    }
    
    var images: [Image] = []
    
    var page = 0
    
    var pageSize = 12
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor(named: Colors.blueColorName)
        refreshControl.isUserInteractionEnabled = false
        return refreshControl
    }()
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .default
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        deleteToast.isHidden = true
        setTable()
        loadGraffities()
        self.navigationController?.navigationBar.barStyle = .black
         
        
        //UIApplication.shared.statusBarStyle = .default //Set Style
        //UIApplication.shared.isStatusBarHidden = false //Set if hidden
        // Do any additional setup after loading the view.
    }
    
    @objc func handleRefresh() {
           refreshControl.endRefreshing()
           images = []
           page = 0
           loadGraffities()
       }
    
    
    override func viewWillAppear(_ animated: Bool) {
        page = 0
        loadGraffities()
    }
    
    
    func setTable() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: gtaffityCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: gtaffityCellReuseIdentifier)
        tableView.register(UINib(nibName: gtaffityActionsCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: gtaffityActionsCellReuseIdentifier)
        self.tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        tableView.addSubview(refreshControl)
    }
    
    func loadGraffities() {
        RestAPI().getGraffities(page: page, page_size: pageSize, callback: {(isOk, images) in
            if isOk, let images = images, !images.isEmpty {
                var newItemsCount = 0
                for image in images {
                    if !self.images.contains(image) {
                        self.images.append(image)
                        newItemsCount += 1
                    }
                }
                if newItemsCount > 0 {
                    self.tableView.reloadData()
                }
            }
        })
    }
    
    @objc func profileButtonDidTap() {
        debugPrint("profile button did tap")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let welcomeController = storyboard.instantiateViewController(withIdentifier: "EditProfileViewController") as? EditProfileViewController else {
            debugPrint("Fail")
            return
            
        }
        
        
        self.navigationController?.pushViewController(welcomeController, animated: true)
    }
    
    @objc func addButtonDidTap() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let addFirstSnapeVC = storyboard.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
        self.navigationController?.pushViewController(addFirstSnapeVC, animated: true)
    }
    

}

extension ListViewController: UITableViewDelegate, UITableViewDataSource {
    // set number of rows to 1 when section is close and to subcomments count + parrent comment when it's open
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return images.count
    }
    // set cells for sections and rows from comments and comment children
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: gtaffityCellReuseIdentifier) as! GraffityTableViewCell
        if indexPath.row < images.count {
                cell.setUp(graffity: images[indexPath.row])
        }
        if indexPath.row == images.count - 1 {
            page += 1
            loadGraffities()
        }
        return cell
    }
    // open and close section when tap on section header or cell from it
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row < images.count {
            let graffity = images[indexPath.row]
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let addFirstSnapeVC = storyboard.instantiateViewController(withIdentifier: "FullImageViewController") as! FullImageViewController
            addFirstSnapeVC.graffity = graffity
           // self.navigationController?.pushViewController(addFirstSnapeVC, animated: true)
            
            self.present(addFirstSnapeVC, animated: true, completion: nil)
            //self.push(addFirstSnapeVC, animated: true, completion: nil)
        }
        
        /*if indexPath.section < images.count {
            if images[indexPath.section].opened == true {
                images[indexPath.section].opened = false
                let sections = IndexSet.init(integer: indexPath.section)
                tableView.reloadSections(sections, with: .fade)
            } else {
                images[indexPath.section].opened = true
                let sections = IndexSet.init(integer: indexPath.section)
                tableView.reloadSections(sections, with: .fade)
            }
        }*/
    }
    
    
   /* func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        // 1
        let editAction = UITableViewRowAction(style: .normal, title: "Edit" , handler: { (action:UITableViewRowAction, indexPath: IndexPath) -> Void in
            // 2
            if indexPath.row < self.images.count {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let addFirstSnapeVC = storyboard.instantiateViewController(withIdentifier: "EditGraffityViewController") as! EditGraffityViewController
                addFirstSnapeVC.graffity = self.images[indexPath.row]
                addFirstSnapeVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(addFirstSnapeVC, animated: true)
            }
            
        })
        editAction.backgroundColor = UIColor(rgb: 0xf98100)
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete" , handler: { (action:UITableViewRowAction, indexPath: IndexPath) -> Void in
            // 2
            let shareMenu = UIAlertController(title: nil, message: "Delete", preferredStyle: .actionSheet)
            
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            shareMenu.addAction(okAction)
            
            self.present(shareMenu, animated: true, completion: nil)
        })
        deleteAction.backgroundColor = UIColor(rgb: 0xf71500)
        // 5
        return [deleteAction, editAction ]
    }*/
    
    
}

extension ListViewController: GraffityActionsTableViewCellDelegate {
    func editButtonDidTap(graffity: Image, cell: GraffityActionsTableViewCell) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let addFirstSnapeVC = storyboard.instantiateViewController(withIdentifier: "EditGraffityViewController") as! EditGraffityViewController
        addFirstSnapeVC.graffity = graffity
        self.present(addFirstSnapeVC, animated: true, completion: nil)
    }
    
    func deleteButtonDidTap(graffity: Image, cell: GraffityActionsTableViewCell) {
//        if let index = images.index(of: graffity) {
//            let indexPath = IndexPath(row: 0, section: index)
//        }
    }
    
    func solvedButtonDidTap(graffity: Image, cell: GraffityActionsTableViewCell) {
//        if let index = images.index(of: graffity) {
//            let indexPath = IndexPath(row: 0, section: index)
//        }
    }
}
