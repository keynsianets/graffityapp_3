//
//  LocationRequestViewController.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/15/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit

class LocationRequestViewController: UIViewController, LocationDelegate {
    
    @IBOutlet var mainBackgroundView: UIView! {
        didSet {
            mainBackgroundView.backgroundColor = UIColor(named: Colors.backgroundName)
        }
    }
    
    @IBOutlet weak var screenTitleLabel: UILabel! {
        didSet {
            screenTitleLabel.text = Strings.accessYour + "\n" + Strings.location
            screenTitleLabel.font = UIFont(name: Fonts.avenirBlack, size: 28)
            screenTitleLabel.textColor = UIColor(named: Colors.textName)
        }
    }
    
    @IBOutlet weak var screenDescriptionLabel: UILabel! {
           didSet {
               screenDescriptionLabel.text = Strings.pleaseEnableLocation
               screenDescriptionLabel.font = UIFont(name: Fonts.avenirRoman, size: 18)
            screenDescriptionLabel.textColor = UIColor(named: Colors.textName)
           }
       }
    
    @IBOutlet weak var allowLocationsButton: UIButton! {
        didSet {
            allowLocationsButton.setTitle(Strings.allowLocation, for: .normal)
            allowLocationsButton.titleLabel?.font = UIFont(name: Fonts.avenirLight, size: 14)
            allowLocationsButton.setTitleColor(Colors.white, for: .normal)
            allowLocationsButton.addTarget(self, action: #selector(allowButtonDidTap), for: .touchUpInside)
            allowLocationsButton.backgroundColor = UIColor(named: Colors.blueColorName)
        }
    }
    
    @IBOutlet weak var doNotAllowButton: UIButton! {
        didSet {
            doNotAllowButton.setTitle(Strings.doNotAllow, for: .normal)
            doNotAllowButton.titleLabel?.font = UIFont(name: Fonts.avenirRoman, size: 16)
            doNotAllowButton.setTitleColor(UIColor(named: Colors.textName), for: .normal)
            doNotAllowButton.addTarget(self, action: #selector(doNotAllowButtonDidTap), for: .touchUpInside)
            doNotAllowButton.backgroundColor = Colors.clear

        }
    }
    @IBOutlet weak var locationPinIcon: UIImageView!
    @IBOutlet weak var cityImage: UIImageView!
    
    var location: LocationManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
         
    }
    
    @objc func allowButtonDidTap() {
        if LocationManager.shared.enabled {
            goNext()
        } else {
            location = LocationManager(delegate: self)
        }
    }
    
    @objc func goNext() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let addFirstSnapeVC = storyboard.instantiateViewController(withIdentifier: "AddFirstSnapeViewController") as! AddFirstSnapeViewController
        self.navigationController?.pushViewController(addFirstSnapeVC, animated: true)
    }
    
    func didChangeAuthorization() {
        goNext()
    }
    
    @objc func doNotAllowButtonDidTap() {
        let tabBar = MainTabBarViewController()
        self.navigationController?.pushViewController(tabBar, animated: true)
    }


}
