//
//  LoginViewController.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/17/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin

fileprivate let forgotPasswordURL = "https://af8hgqk79h.execute-api.eu-west-1.amazonaws.com/dev/accounts/password_reset/"

class LoginViewController: UIViewController {
    
    @IBOutlet var mainBackgroundView: UIView! {
        didSet {
            mainBackgroundView.backgroundColor = UIColor(named: Colors.backgroundName)
        }
    }
    
    @IBOutlet weak var signInFailedView: UIView!  {
           didSet {
               signInFailedView.backgroundColor = Colors.scarlet
           }
       }
    
    @IBOutlet weak var appNameLabel: UILabel! {
        didSet {
            appNameLabel.text = Strings.appName
            appNameLabel.font = UIFont(name: Fonts.avenirLight, size: 36)
            appNameLabel.textColor = UIColor(named: Colors.blueColorName)
        }
    }
    
    @IBOutlet weak var helloLabel: UILabel! {
        didSet {
            helloLabel.text = Strings.hello + "!"
            helloLabel.font = UIFont(name: Fonts.avenirLight, size: 24)
            helloLabel.textColor = UIColor(named: Colors.textName)
        }
    }
    
    @IBOutlet weak var screenDescriptionLabel: UILabel! {
        didSet {
            screenDescriptionLabel.text = Strings.signInToAccessYourAccount
            screenDescriptionLabel.font = UIFont(name: Fonts.avenirLight, size: 18)
            screenDescriptionLabel.textColor = UIColor(named: Colors.textName)
        }
    }
    
    @IBOutlet weak var mailTextField: ErrorTextFieldWithIcons! {
        didSet {
            mailTextField.placeholder = Strings.emailAddress
        }
    }
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var passwordTextField: ErrorTextFieldWithIcons!  {
        didSet {
            passwordTextField.placeholder = Strings.password
        }
    }
    
    @IBOutlet weak var signInButton: UIButton! {
        didSet {
            signInButton.layer.cornerRadius = 4
            signInButton.layer.masksToBounds = true
            signInButton.addTarget(self, action: #selector(signInButtonDidTap), for: .touchUpInside)
            signInButton.setTitle(Strings.signIn, for: .normal)
            signInButton.titleLabel?.font = UIFont(name: Fonts.avenirRoman, size: 16)
            signInButton.backgroundColor = UIColor(named: Colors.blueColorName)
        }
    }
    
    @IBOutlet weak var facebookSignInButton: UIButton! {
        didSet {
            facebookSignInButton.addTarget(self, action: #selector(getFacebookUserInfo), for: .touchUpInside)
            facebookSignInButton.setTitle(Strings.signInWithFacebook, for: .normal)
            facebookSignInButton.titleLabel?.font = UIFont(name: Fonts.avenirRoman, size: 16)
            facebookSignInButton.setTitleColor(Colors.white, for: .normal)

        }
    }
    
    @IBOutlet weak var facebookContainerView: UIView! {
        didSet {
            facebookContainerView.layer.cornerRadius = 4
            facebookContainerView.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var facebookIcon: UIImageView!
    @IBOutlet weak var lockIcon: UIImageView!
    @IBOutlet weak var mailIcon: UIImageView!
    
    
    @IBOutlet weak var registerButton: UIButton! {
        didSet {
            registerButton.setTitle(Strings.registerHere, for: .normal)
            registerButton.titleLabel?.font = UIFont(name: Fonts.avenirLight, size: 14)
            registerButton.setTitleColor(UIColor(named: Colors.blueColorName), for: .normal)
            registerButton.addTarget(self, action: #selector(registerButtonDidTap), for: .touchUpInside)
            
        }
    }
    @IBOutlet weak var forgotPasswordButton: UIButton! {
        didSet {
            forgotPasswordButton.setTitle(Strings.forgotPassword + "?", for: .normal)
            forgotPasswordButton.titleLabel?.font = UIFont(name: Fonts.avenirLightOblique, size: 12)
            forgotPasswordButton.setTitleColor(Colors.white, for: .normal)
            forgotPasswordButton.addTarget(self, action: #selector(forgotPasswordButtonDidTap), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var dontHaveAccountLabel: UILabel! {
        didSet {
            dontHaveAccountLabel.text = Strings.dontHaveAccount
            dontHaveAccountLabel.font = UIFont(name: Fonts.avenirLight, size: 14)
            dontHaveAccountLabel.textColor = UIColor(named: Colors.textName)
        }
    }
    
    @IBOutlet weak var signInFailed: UILabel! {
        didSet {
            signInFailed.text = Strings.signInFailed + "!"
            signInFailed.font = UIFont(name: Fonts.avenirRoman, size: 14)
            signInFailed.textColor = Colors.white
        }
    }
    
    @IBOutlet weak var usernameAndPasswordDidntMatch: UILabel!  {
        didSet {
            usernameAndPasswordDidntMatch.text = Strings.usernameAndPasswordDidntMatch
            usernameAndPasswordDidntMatch.font = UIFont(name: Fonts.avenirRoman, size: 14)
            usernameAndPasswordDidntMatch.textColor = Colors.white
        }
    }
    
    
    lazy var duplcateView: DropdownView = {
        let view = DropdownView()
        return view
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @objc func getFacebookUserInfo(){
        self.activityIndicator.startAnimating()
        debugPrint("call getFacebookUserInfo()")
        let loginManager = LoginManager()
        loginManager.defaultAudience = .onlyMe
        loginManager.logIn(permissions: [ .publicProfile, .email ], viewController: self) { loginResult in
            debugPrint("call getFacebookUserInfo() result")
            
            switch loginResult {
            case .cancelled:
                print("Cancel button click")
                self.activityIndicator.stopAnimating()
            case .success( _, _, let authenticationToken):
                debugPrint("FBTOKEN: ", authenticationToken.tokenString)
                RestAPI().signIn(facebookToken: authenticationToken.tokenString, callback: { (isOk, token) in
                    debugPrint("FB sign in api answer")
                    if let token = token {
                        debugPrint("TOKEN: ", token)
                        UserProfile.shared.token = token
                        RestAPI().getUserInfo(callback: { (isOk, info) in
                            self.activityIndicator.stopAnimating()
                            if isOk, let info = info {
                                if let email = info.email, !email.isEmpty {
                                    self.gotoWelcomeVC()
                                } else if let username = info.username, !username.isEmpty {
                                    self.gotoWelcomeVC()
                                } else {
                                    self.duplcateView.showMessage(message: Strings.signUpFailed + "\n" + Strings.email + Strings.fieldCannotBeBlank, completion: nil)
                                }
                            } else {
                                self.showAlertOk(Strings.error, andText: Strings.somethingWrongTryAgain)
                            }
                        })
                        
                    } else {
                        self.activityIndicator.stopAnimating()
                        self.showAlertOk(Strings.error, andText: Strings.somethingWrongTryAgain)
                    }
                })
                //}
            //Connection.start()
            default:
                print("??")
            }
        }
    }
    
    @objc func signInButtonDidTap() {
        
        view.endEditing(true)
        
        guard let email = mailTextField.text else {
            return
        }
        guard let password = passwordTextField.text else {
            return
        }
        
        if email == "" {  duplcateView.showMessage(message: Strings.signUpFailed + "\n" + Strings.email + Strings.fieldCannotBeBlank, completion: nil); return}
        if password == "" {  duplcateView.showMessage(message: Strings.signUpFailed + "\n" + Strings.password + Strings.fieldCannotBeBlank, completion: nil); return}
        if password.count < 8 {  duplcateView.showMessage(message: Strings.signUpFailed + "\n" + Strings.ensurePasswordHasBeenLeast8Characters, completion: nil); return}
        
        
        self.activityIndicator.startAnimating()
        RestAPI().signIn(userName: email, password: password) { (isOk, token) in
            self.activityIndicator.stopAnimating()
            if let token = token {
                
                UserProfile.shared.token = token
                self.gotoWelcomeVC()
                
            } else {
                self.showAlertOk(Strings.error, andText: Strings.wrongEmailOrPassword)
            }
        }
        
        
        
        
    }
    
    func gotoWelcomeVC() {
        if !UserProfile.shared.notNeedToShowWelcomeScreens  {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let welcomeController = storyboard.instantiateViewController(withIdentifier: "WelcomeViewController") as? WelcomeViewController else {
                debugPrint("Fail")
                return
            }
            self.navigationController?.pushViewController(welcomeController, animated: true)
        } else {
            let tabBar = MainTabBarViewController()
            self.navigationController?.pushViewController(tabBar, animated: true)
        }
        
    }
    
    
    
    @objc func registerButtonDidTap() {
        let vcs = self.navigationController?.viewControllers
        if ((vcs?.contains(self)) != nil) {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func forgotPasswordButtonDidTap() {
        if let url = URL(string: forgotPasswordURL), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        } else {
            print("ooops, I can`t open url: \(forgotPasswordURL)")
        }
    }
    
    
}
