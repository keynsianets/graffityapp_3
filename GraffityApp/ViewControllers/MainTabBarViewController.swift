//
//  MainTabBarViewController.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/19/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import Foundation
import ESTabBarController_swift
import Material

class MainTabBarViewController: ESTabBarController {
    var tabBarHiding = true
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(dismissTabBar), name: .dissmis_tab_bar, object: nil)
        
        (UIApplication.shared.delegate as! AppDelegate).mainTabBarVC = self
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let listViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        listViewController.tabBarItem.tag = 0
        
        let cameraViewController = storyboard.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
        cameraViewController.tabBarItem.tag = 1
        
        let editProfileViewController = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
         editProfileViewController.tabBarItem.tag = 2
        
        //  let exVC = ViewControllerForTests()

        self.shouldHijackHandler = {
            tabbarController, viewController, index in
            if index == 1 {
                return true
            }
            return false
        }
        self.didHijackHandler = {
            [weak tabBarController] tabbarController, viewController, index in
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                if let vc = self.navigationController?.topViewController, vc.isKind(of: CameraViewController.self) { return }
                self.navigationController?.pushViewController(cameraViewController, animated: true)
                
            }
            
        }
        
        let v1 = listViewController
        let v2 = cameraViewController
        let v3 = editProfileViewController
        
        let leftContentView = ExampleIrregularityBasicContentView()
        leftContentView.insets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: -15)
        
        let rightContentView = ExampleIrregularityBasicContentView()
        rightContentView.insets = UIEdgeInsets(top: 0, left: -15, bottom: 0, right: 15)
        
        v1.tabBarItem = ESTabBarItem.init(leftContentView, title: nil, image: #imageLiteral(resourceName: "nounList1714235"), selectedImage: #imageLiteral(resourceName: "nounList1714235"))

        v2.tabBarItem = ESTabBarItem.init(ExampleIrregularityContentView(), title: nil, image: #imageLiteral(resourceName: "icoNavPostLarge"), selectedImage: #imageLiteral(resourceName: "icoNavPostLarge"))
        v3.tabBarItem = ESTabBarItem.init(rightContentView, title: nil, image: #imageLiteral(resourceName: "icon"), selectedImage:  #imageLiteral(resourceName: "icon"))
        
        //tabBar.selectionIndicatorImage = getImageWithColorPosition(color: UIColor.blue, size: CGSize(width:(self.view.frame.size.width)/5,height: 49), lineSize: CGSize(width:(self.view.frame.size.width)/5, height:2))
        tabBar.isTranslucent = false
        
        viewControllers = [v1, v2, v3].map { AppNavigationController(rootViewController: $0) }

        tabBar.barTintColor =  UIColor(named: Colors.tabBarBackgroundName)
    }
    
    func getImageWithColorPosition(color: UIColor, size: CGSize, lineSize: CGSize) -> UIImage {
        let rect = CGRect(x:0, y: 0, width: size.width, height: size.height)
        let rectLine = CGRect(x:0, y:size.height-lineSize.height,width: lineSize.width,height: lineSize.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        UIColor.clear.setFill()
        UIRectFill(rect)
        color.setFill()
        UIRectFill(rectLine)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return image
    }
    
    @objc func dismissTabBar() {
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}

class AppNavigationController: NavigationController {
    open override func prepare() {
        super.prepare()
        isMotionEnabled = true
        
        guard let v = navigationBar as? NavigationBar else {
            return
        }
        v.isHidden = true

        
        //v.backgroundColor = .white
        //v.depthPreset = .none
        //v.dividerColor = Color.grey.lighten2
    }
}



class ExampleIrregularityContentView: ESTabBarItemContentView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.imageView.layer.masksToBounds = false
        self.insets = UIEdgeInsets(top: -15, left: 0, bottom: 0, right: 0)
        let transform = CGAffineTransform.identity
        self.imageView.transform = transform
        self.imageView.contentMode = .scaleAspectFill
        self.superview?.bringSubviewToFront(self)
        textColor = UIColor.init(white: 255.0 / 255.0, alpha: 1.0)
        highlightTextColor = UIColor.init(white: 255.0 / 255.0, alpha: 1.0)
        iconColor = UIColor.init(white: 255.0 / 255.0, alpha: 1.0)
        highlightIconColor = UIColor.init(white: 255.0 / 255.0, alpha: 1.0)
        backdropColor = .clear
        highlightBackdropColor = .clear
        
        renderingMode = .alwaysOriginal
    }
   
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("coder not implement")
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let p = CGPoint.init(x: point.x - imageView.frame.origin.x, y: point.y - imageView.frame.origin.y)
        return sqrt(pow(imageView.bounds.size.width / 2.0 - p.x, 2) + pow(imageView.bounds.size.height / 2.0 - p.y, 2)) < imageView.bounds.size.width / 2.0
    }
    
    override func updateLayout() {
        super.updateLayout()
        let centerPoint = CGPoint.init(x: self.bounds.size.width / 2.0, y: self.bounds.size.height / 2.0)
        self.imageView.frame = CGRect(center: centerPoint, size: CGSize(width: 74, height: 74))
    }
    
    
}


class ExampleIrregularityBasicContentView: ExampleBouncesContentView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        renderingMode = .alwaysTemplate
        
        //iconColor = .lightGray
        imageView.backgroundColor =  UIColor(named: Colors.tabBarBackgroundName) ?? Colors.white
        highlightIconColor = UIColor(named: Colors.blueColorName) ?? Colors.darkSkyBlue

    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("coder not implement")
    }
    
    override func updateLayout() {
        super.updateLayout()
        let centerPoint = CGPoint.init(x: self.bounds.size.width / 2.0, y: self.bounds.size.height / 2.0)
        self.imageView.contentMode = .scaleAspectFit
        self.imageView.frame = CGRect(center: centerPoint, size: CGSize(width: 33, height: 30))
    }
}

class ExampleBouncesContentView: ExampleBasicContentView {
    
    public var duration = 0.3
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor(named: Colors.tabBarBackgroundName)

    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("coder not implement")
    }
    
    override func selectAnimation(animated: Bool, completion: (() -> ())?) {
        self.bounceAnimation()
        completion?()
    }
    
    override func reselectAnimation(animated: Bool, completion: (() -> ())?) {
        self.bounceAnimation()
        completion?()
    }
    
    func bounceAnimation() {
        let impliesAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        impliesAnimation.values = [1.0 ,1.4, 0.9, 1.15, 0.95, 1.02, 1.0]
        impliesAnimation.duration = duration * 2
        impliesAnimation.calculationMode = CAAnimationCalculationMode.cubic
        imageView.layer.add(impliesAnimation, forKey: nil)
    }
}

class ExampleBasicContentView: ESTabBarItemContentView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        renderingMode = .alwaysTemplate
        textColor = UIColor.init(white: 0, alpha: 1.0)
        highlightTextColor = UIColor(named: Colors.blueColorName) ?? Colors.darkSkyBlue
        iconColor = UIColor.init(white: 175.0 / 255.0, alpha: 1.0)
        highlightIconColor = UIColor(named: Colors.blueColorName) ?? Colors.darkSkyBlue

    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("coder not implement")
    }
    
    
}
