//
//  MapViewController.swift
//  GraffityApp
//
//  Created by Denis Markov on 9/20/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit
import GoogleMaps

fileprivate let gtaffityCellReuseIdentifier = "GraffityCollectionViewCell"
fileprivate let gtaffityActionsCellReuseIdentifier = "GraffityActionsTableViewCell"

class MapViewController: UIViewController {
    
    let locationManager = CLLocationManager()
    private var mapView: GMSMapView?
    private var clusterManager: GMUClusterManager?
    var mapItems: [POIItem] = []
    var newPoints: [POIItem] = []
    
    @IBOutlet var mainBackgroundView: UIView! {
        didSet {
            mainBackgroundView.backgroundColor = UIColor(named: Colors.backgroundName)
        }
    }
    
    @IBOutlet weak var graffitiListView: UIView! {
        didSet {
            graffitiListView.isHidden = false
        }
    }
    @IBOutlet weak var graffitiListViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var graffitiListTable: UITableView!
    @IBOutlet weak var graffitiListViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var currentPoint: POIItem? {
        didSet {
            if let newValue = currentPoint {
                setListView(hidden: false)
                dataSource = []
                for graf in newValue.graffiti {
                    if !dataSource.contains(graf) {
                        dataSource.append(graf)
                    }
                }
            } else {
                setListView(hidden: true)
                dataSource = []
            }
            collectionView.reloadData()
        }
    }
    var currrentCluster: [POIItem]? {
        didSet {
            if let newValue = currrentCluster {
                setListView(hidden: false)
                dataSource = []
                for item in newValue {
                    for graf in item.graffiti {
                        if !dataSource.contains(graf) {
                            dataSource.append(graf)
                        }
                    }
                }
            } else {
                setListView(hidden: true)
                dataSource = []
            }
            collectionView.reloadData()
        }
    }
    
    var dataSource: [Image] = []
    
    var minLat: Double = 0
    var minLng : Double = 0
    var maxLat: Double = 0
    var maxLng: Double = 0
    
    var bottomViewShowCenter: CGPoint?
    var bottomViewHideCenter: CGPoint?
    let bottomViewShowY = UIScreen.main.bounds.height * 0.6 - 50
    let bottomViewHideY = UIScreen.main.bounds.height
    let bottomViewHeight = UIScreen.main.bounds.height * 0.4
    
    private var debounce = Debouncer()
    var bottomViewHidden = true
    var box: String = ""
    
    var minX: Double = 0
    var maxX: Double = 0
    var minY: Double = 0
    var maxY: Double = 0
    
    var firstLoadPoints = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        graffitiListViewBottomConstraint.constant = -self.bottomViewHeight
//        graffitiListViewHeightConstraint.constant = bottomViewHeight
//        setTable()
        collectionView.register(UINib(nibName: gtaffityCellReuseIdentifier, bundle: nil), forCellWithReuseIdentifier: gtaffityCellReuseIdentifier)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    override func loadView() {
        super.loadView()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        } else {
            self.showAlertOk(Strings.haveNoAccessToLocation, andText: Strings.youHaveToAllowApplicationToUseLocation)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        graffitiListViewBottomConstraint.constant = -self.bottomViewHeight
        bottomViewHidden = true
        guard let hide = bottomViewHideCenter else { return }
        self.graffitiListView.center = hide
    }
    
    func setListView(hidden: Bool) {
        debugPrint(#function, hidden)
        if hidden == bottomViewHidden { return }
        if !hidden { self.graffitiListViewBottomConstraint.constant = 0 }
        UIView.animate(withDuration: 0.6, animations: {
            self.graffitiListView.frame.origin.y = hidden ? self.bottomViewHideY : self.bottomViewShowY
        }) { (_) in
            if hidden { self.graffitiListViewBottomConstraint.constant = -self.bottomViewHeight }
        }
        bottomViewHidden = hidden
    }
    
    func setTable() {
        graffitiListTable.delegate = self
        graffitiListTable.dataSource = self
        graffitiListTable.register(UINib(nibName: gtaffityCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: gtaffityCellReuseIdentifier)
        graffitiListTable.register(UINib(nibName: gtaffityActionsCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: gtaffityActionsCellReuseIdentifier)
        graffitiListTable.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 50, right: 0)
    }
    
    func loadPoints(mapView: GMSMapView) {
        let mapProjection = mapView.projection.visibleRegion()
        let bounds = GMSCoordinateBounds(region: mapProjection)
        let northEast = bounds.northEast
        let southWest = bounds.southWest
        if checkBounds(northEast: northEast, southWest: southWest) {
            debugPrint("load points")
            
            let xMin = min(northEast.longitude, southWest.longitude)
            let xMax = max(northEast.longitude, southWest.longitude)
            let yMin = min(northEast.latitude, southWest.latitude)
            let yMax = max(northEast.latitude, southWest.latitude)
            var needUpdate = false
            if firstLoadPoints {
                firstLoadPoints = false
                needUpdate = true
                minX = xMin
                maxX = xMax
                minY = yMin
                maxY = yMax
            }
            if xMin < minX {
                minX = xMin
                needUpdate = true
            }
            if xMax > maxX {
                maxX = xMax
                needUpdate = true
            }
            if yMin < minY {
                minY = yMin
                needUpdate = true
            }
            if yMax < maxY {
                maxY = yMax
                needUpdate = true
            }
            if needUpdate {
                box = "\(minX),\(minY),\(maxX),\(maxY)"
                self.debounce.debounce(delay: .milliseconds(100), action: callAPILoadPointsWithDebounce)()
            }
            
        }
    }
    
    func callAPILoadPointsWithDebounce() -> Void {
        RestAPI().getPointsForCameraBounds(box: box, page_size: 100) { (isOk, points) in
            if isOk, let points = points {
                self.preparePoints(points: points)
            }
        }
    }
    
    var firstLoad = true
    
    func checkBounds(northEast: CLLocationCoordinate2D, southWest: CLLocationCoordinate2D) -> Bool {
        if firstLoad {
            minLat = northEast.latitude
            minLng = northEast.longitude
            maxLat = southWest.latitude
            maxLng = southWest.longitude
            firstLoad = false
            return true
        }
        if northEast.latitude < minLat {
            minLat = northEast.latitude
            return true
        }
        if northEast.longitude < minLng {
            minLng = northEast.longitude
            return true
        }
        if southWest.latitude > maxLat {
            maxLat = southWest.latitude
            return true
        }
        if southWest.longitude > maxLng {
            maxLng = southWest.longitude
            return true
        }
        return false
    }
    
    func preparePoints(points: [Image]) {
        for point in points {
            if let loc = point.location, let lat = loc.lat, let lng = loc.lng, let id = point.id {
                let item = POIItem(position: CLLocationCoordinate2D(latitude: filterPointCoordinate(source: lat, lat: true), longitude: filterPointCoordinate(source: lng, lat: false)), name: point.title ?? Settings.noTitle, id: id, image: point)
                if !mapItems.contains(item) {
                    newPoints.append(item)
                    //
                } else {
                    if let index = mapItems.index(of: item) {
                        mapItems[index].graffiti.append(point)
                    }
                }
            }
        }
        self.displayPoints()
    }
    
    func displayPoints() {
        
        guard let cluster = clusterManager else { return }
        for point in newPoints {
            if !mapItems.contains(point) {
                mapItems.append(point)
                cluster.add(point)
            }
        }
        newPoints = []
        debugPrint("dots count: ", mapItems.count)
        cluster.cluster()
    }
    
    func filterPointCoordinate(source: Double, lat: Bool) -> Double {
        if source == 0 { return 0.0000001 }
        return source
    }
}

extension MapViewController: GMUClusterManagerDelegate {
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        var newCluster: [POIItem] = []
        for item in cluster.items {
            if let myItem = item as? POIItem {
                newCluster.append(myItem)
            }
        }
        currrentCluster = newCluster == self.currrentCluster ? nil : newCluster
        return false
    }
}

extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        if self.mapView != nil, self.clusterManager != nil { return }
        locationManager.stopUpdatingLocation()
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        let camera = GMSCameraPosition.camera(withLatitude: locValue.latitude, longitude: locValue.longitude, zoom: 6.0)
        let mapView = GMSMapView.map(withFrame: view.bounds, camera: camera)
        mapView.isUserInteractionEnabled = true
        self.mapView = mapView
        mapView.delegate = self
        loadPoints(mapView: mapView)
        //iew.isUserInteractionEnabled = false
        view.addSubview(mapView)
        view.sendSubviewToBack(mapView)
        
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView,
                                                 clusterIconGenerator: iconGenerator)
        let clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm,
                                               renderer: renderer)
        clusterManager.setDelegate(self, mapDelegate: self)
        self.clusterManager = clusterManager
        self.clusterManager?.setDelegate(self, mapDelegate: self)
        displayPoints()
        
    }
}

extension MapViewController: GMSMapViewDelegate {
    // MARK: - GMUMapViewDelegate
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        // debugPrint("did tap some marker")
        if let poiItem = marker.userData as? POIItem {
            self.currentPoint = poiItem == self.currentPoint ? nil : poiItem
        } else {
            debugPrint("Did tap a normal marker")
        }
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        // debugPrint("map did change")
        loadPoints(mapView: mapView)
    }
    
    
}


// Point of Interest Item which implements the GMUClusterItem protocol.
class POIItem: NSObject, GMUClusterItem {
    
    var id: String
    var position: CLLocationCoordinate2D
    var name: String!
    var graffiti: [Image] = []
    
    init(position: CLLocationCoordinate2D, name: String, id: String, image: Image) {
        graffiti.append(image)
        self.position = position
        self.name = name
        self.id = id
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let secondId = (object as? POIItem)?.id else { return false }
        return id == secondId
    }
    
}

extension MapViewController: UITableViewDelegate, UITableViewDataSource {
    // set number of rows to 1 when section is close and to subcomments count + parrent comment when it's open
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    // set cells for sections and rows from comments and comment children
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: gtaffityCellReuseIdentifier) as! GraffityTableViewCell
        if indexPath.row < dataSource.count {
            cell.setUp(graffity: dataSource[indexPath.row])
        }
        return cell
    }
    // open and close section when tap on section header or cell from it
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension MapViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: gtaffityCellReuseIdentifier, for: indexPath) as! GraffityCollectionViewCell

        if indexPath.item < dataSource.count {
            let width = UIScreen.main.bounds.width * 0.7
            cell.setUp(graffity: dataSource[indexPath.row], width: width)
        }
        return cell
    }
    
    
}

extension MapViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.width * 0.7 + 5
//        let height = collectionView.bounds.height
        return CGSize(width: width, height: 140)
    }
}
