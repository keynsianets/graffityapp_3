//
//  SignUpViewController.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/17/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit
import FacebookLogin
import FBSDKLoginKit

class SignUpViewController: UIViewController {
    
    lazy var duplcateView: DropdownView = {
        let view = DropdownView()
        return view
    }()
    
    @IBOutlet var mainBackgroundView: UIView! {
        didSet {
            mainBackgroundView.backgroundColor = UIColor(named: "backgroundColor")
        }
    }
    
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var contentView: UIView!
    
    
    
    @IBOutlet weak var newAccountLabel: UILabel! {
        didSet {
            newAccountLabel.text = Strings.newAccount
            newAccountLabel.font = UIFont(name: Fonts.avenirLight, size: 24)
            newAccountLabel.textColor = UIColor(named: Colors.textName)
        }
    }
    
    @IBOutlet weak var completeRegistrationLabel: UILabel! {
        didSet {
            completeRegistrationLabel.text = Strings.completeRegistration
            completeRegistrationLabel.font = UIFont(name: Fonts.avenirLight, size: 18)
            completeRegistrationLabel.textColor = UIColor(named: Colors.textName)
        }
    }
    
    @IBOutlet weak var userNameTextField: ErrorTextFieldWithIcons!  {
        didSet {
            userNameTextField.placeholder = Strings.userName
            userNameTextField.placeholderLabel.font = UIFont(name: Fonts.avenirBook, size: 16)
            userNameTextField.font = UIFont(name: Fonts.avenirBook, size: 16)
        }
    }
    
    
    @IBOutlet weak var fullNameTextField: ErrorTextFieldWithIcons!  {
        didSet {
            fullNameTextField.placeholder = Strings.fullName
            fullNameTextField.placeholderLabel.font = UIFont(name: Fonts.avenirBook, size: 16)
            fullNameTextField.font = UIFont(name: Fonts.avenirBook, size: 16)
        }
    }
    
    @IBOutlet weak var emailTextField: ErrorTextFieldWithIcons!  {
        didSet {
            emailTextField.placeholder = Strings.email
            emailTextField.placeholderLabel.font = UIFont(name: Fonts.avenirBook, size: 16)
            emailTextField.font = UIFont(name: Fonts.avenirBook, size: 16)
        }
    }
    
    
    @IBOutlet weak var passwordTextField: ErrorTextFieldWithIcons!  {
        didSet {
            passwordTextField.placeholder = Strings.password
            passwordTextField.placeholderLabel.font = UIFont(name: Fonts.avenirBook, size: 16)
            passwordTextField.font = UIFont(name: Fonts.avenirBook, size: 16)
        }
    }
    
    @IBOutlet weak var registerButton: UIButton! {
        didSet {
            registerButton.layer.cornerRadius = 4
            registerButton.layer.masksToBounds = true
            registerButton.addTarget(self, action: #selector(registerButtonDidTap), for: .touchUpInside)
            registerButton.setTitle(Strings.register, for: .normal)
            registerButton.titleLabel?.font = UIFont(name: Fonts.avenirRoman, size: 16)
            registerButton.setTitleColor(.white, for: .normal)
            registerButton.backgroundColor = UIColor(named: Colors.blueColorName)
        }
    }
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var facebookSignUpButton: UIButton! {
        didSet {
            facebookSignUpButton.addTarget(self, action: #selector(getFacebookUserInfo), for: .touchUpInside)
            facebookSignUpButton.setTitle(Strings.signUpWithFacebook, for: .normal)
            facebookSignUpButton.titleLabel?.font = UIFont(name: Fonts.avenirRoman, size: 16)
            facebookSignUpButton.setTitleColor(.white, for: .normal)

        }
    }
    
    @IBOutlet weak var facebookIcon: UIImageView!
    
    @IBOutlet weak var facebookContainerView: UIView! {
        didSet {
            facebookContainerView.layer.cornerRadius = 4
            facebookContainerView.layer.masksToBounds = true
        }
    }
    
    @IBOutlet weak var iAlreadyHaveAccountLabel: UILabel! {
        didSet {
            guard let fontText = UIFont(name: Fonts.avenirLight, size: 14.0) else {return}
            let attributedString = NSMutableAttributedString(string: Strings.iAlreadyHaveAccount, attributes: [
                .font: fontText,
                .foregroundColor: UIColor(named: Colors.textName) as Any,
                .kern: 0.39
            ])
            attributedString.addAttribute(.foregroundColor, value: UIColor(named: Colors.blueColorName), range: NSRange(location: 27, length: 5))
            iAlreadyHaveAccountLabel.attributedText = attributedString
            iAlreadyHaveAccountLabel.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(loginButtonDidTap))
            iAlreadyHaveAccountLabel.addGestureRecognizer(tap)
        }
    }
    
    @IBOutlet weak var mailIcon: UIImageView!
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var fullNameUserIcon: UIImageView!
    @IBOutlet weak var lockIcon: UIImageView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let _ = UserProfile.shared.token {
            gotoWelcomeVC()
        }
        /*
         // Do any additional setup after loading the view.*/
        
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.updateViewConstraints()

    }
    
    @objc func getFacebookUserInfo(){
        
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [.publicProfile, .email ], viewController: self) { (result) in
            switch result{
            case .cancelled:
                print("Cancel button click")
            case .success:
                let params = ["fields" : "id, name, first_name, last_name, picture.type(large), email "]
                let graphRequest = GraphRequest.init(graphPath: "/me", parameters: params)
                let Connection = GraphRequestConnection()
                self.activityIndicator.startAnimating()
                Connection.add(graphRequest) { (Connection, result, error) in
                    let info = result as! [String : AnyObject]
                    print(info["name"] as! String)
                    print(graphRequest.tokenString ?? "no token")
                    self.activityIndicator.stopAnimating()
                    
                    
                    UserProfile.shared.email = info["email"] as? String
                    UserProfile.shared.first_name = info["first_name"] as? String
                    UserProfile.shared.last_name = info["last_name"] as? String
                    UserProfile.shared.full_name = info["name"] as? String
                    UserProfile.shared.fb_token = graphRequest.tokenString
                    
                    
                    
                    self.gotoFacebookSignUP()
                    
                    
                    
                    
                    
                }
                Connection.start()
            default:
                print("??")
            }
        }
    }
    
    @objc func registerButtonDidTap() {
        debugPrint("register")
        
        view.endEditing(true)
        
        guard let email = emailTextField.text else {
            return
        }
        
        guard let username = userNameTextField.text else  {
            return
        }
        
        guard let password = passwordTextField.text else {
            return
        }
        
        
        guard let fullName = fullNameTextField.text else {
            return
        }
        
        if username == "" {  duplcateView.showMessage(message: Strings.signUpFailed + "\n" + Strings.userName + Strings.fieldCannotBeBlank, completion: nil); return}
        if fullName == "" {  duplcateView.showMessage(message: Strings.signUpFailed + "\n" + Strings.fullName + Strings.fieldCannotBeBlank, completion: nil); return}
        if email == "" {  duplcateView.showMessage(message: Strings.signUpFailed + "\n" + Strings.email + Strings.fieldCannotBeBlank, completion: nil); return}
        if password == "" {  duplcateView.showMessage(message: Strings.signUpFailed + "\n" + Strings.password + Strings.fieldCannotBeBlank, completion: nil); return}
        
        if password.count < 8 {  duplcateView.showMessage(message: Strings.signUpFailed + "\n" + Strings.ensurePasswordHasBeenLeast8Characters, completion: nil); return}
        
        
        self.activityIndicator.startAnimating()
        RestAPI().register(userName: username,  email: email, password: password, full_name: fullName, callback: {
            (isOk, error) in
            
            if isOk {
                UserProfile.shared.username = username
                UserProfile.shared.password = password
                UserProfile.shared.email = email
                RestAPI().signIn(userName: username, password: password, callback: { (isOk, token) in
                    self.activityIndicator.stopAnimating()
                    if let token = token {
                        UserProfile.shared.token = token
                        self.gotoWelcomeVC()
                    } else {
                        self.showAlertOk(Strings.error, andText: Strings.somethingWrongTryAgain)
                    }
                })
                
                
            } else {
                self.activityIndicator.stopAnimating()
                if let error = error {
                    //duplicate username or email
                    self.showDuplicateError(error: error.getCommonError())
                }  else {
                    self.showAlertOk(Strings.error, andText: Strings.somethingWrongTryAgain)
                }
            }
        })
        
        //        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //        guard let welcomeController = storyboard.instantiateViewController(withIdentifier: "FacebookSignUpViewController") as? FacebookSignUpViewController else {
        //            debugPrint("Fail")
        //            return
        //
        //        }
        //        self.navigationController?.pushViewController(welcomeController, animated: true)
    }
    
    
    
    func gotoFacebookSignUP() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let welcomeController = storyboard.instantiateViewController(withIdentifier: "FacebookSignUpViewController") as? FacebookSignUpViewController else {
            debugPrint("Fail")
            return
            
        }
        self.navigationController?.pushViewController(welcomeController, animated: true)
    }
    
    @objc func backButtonDidTap() {
        let vcs = self.navigationController?.viewControllers
        if ((vcs?.contains(self)) != nil) {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func loginButtonDidTap() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let welcomeController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else {
            debugPrint("Fail")
            return
            
        }
        self.navigationController?.pushViewController(welcomeController, animated: true)
    }
    
    func showDuplicateError(error: String){
        duplcateView.showMessage(message:error, completion: nil)
    }
    
    
    func gotoWelcomeVC() {
        if !UserProfile.shared.notNeedToShowWelcomeScreens  {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let welcomeController = storyboard.instantiateViewController(withIdentifier: "WelcomeViewController") as? WelcomeViewController else {
                debugPrint("Fail")
                return
            }
            self.navigationController?.pushViewController(welcomeController, animated: true)
        } else {
            let tabBar = MainTabBarViewController()
            self.navigationController?.pushViewController(tabBar, animated: true)
        }
        
    }
}
