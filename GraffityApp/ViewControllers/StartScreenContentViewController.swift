//
//  StartScreenContentViewController.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/14/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit

class StartScreenContentViewController: UIViewController {

    @IBOutlet var backgroundView: UIView! {
        didSet {
            backgroundView.backgroundColor = UIColor(named: Colors.backgroundName)
        }
    }
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.font = UIFont(name: Fonts.avenirHeavy, size: 22)
            titleLabel.textColor = UIColor(named: Colors.textName)
        }
    }
    
    @IBOutlet weak var descriptionLabel: UILabel! {
           didSet {
               descriptionLabel.font = UIFont(name: Fonts.avenirBook, size: 22)
            descriptionLabel.textColor = UIColor(named: Colors.textName)
           }
       }
    
    var size: CGSize?
    
    override func viewDidLoad() {
        super.viewDidLoad()
         
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidLayoutSubviews() {
        guard let size = size else { return }
        let frame = CGRect(origin: self.view.bounds.origin, size: size)
        self.view.frame = frame
    }
    
    func setContent(image: UIImage, title: String, description: String, size: CGSize) {
        self.size = size
        imageView.image = image
        titleLabel.text = title
        descriptionLabel.text = description
    }
    


}
