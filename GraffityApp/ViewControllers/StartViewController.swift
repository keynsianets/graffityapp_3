//
//  StartViewController.swift
//  GraffityApp
//
//  Created by Alex Suvorov on 8/30/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {
    
    
    @IBOutlet weak var signUpButton: UIButton! {
        didSet {
            signUpButton.layer.cornerRadius = 4
            signUpButton.layer.masksToBounds = true
            signUpButton.addTarget(self, action: #selector(signUpButtonDidTap), for: .touchUpInside)
            signUpButton.setTitle(Strings.signUp, for: .normal)
            signUpButton.backgroundColor = UIColor(named: Colors.blueColorName)
            signUpButton.setTitleColor(Colors.white, for: .normal)
            signUpButton.titleLabel?.font = UIFont(name: Fonts.avenirRoman, size: 16)
        }
    }
    
    @IBOutlet weak var loginButton: UIButton! {
        didSet {
            loginButton.addTarget(self, action: #selector(loginButtonDidTap), for: .touchUpInside)
            loginButton.setTitle(Strings.login, for: .normal)
            loginButton.backgroundColor = Colors.clear
            loginButton.setTitleColor(Colors.white, for: .normal)
            loginButton.titleLabel?.font = UIFont(name: Fonts.avenirBlack, size: 16)
        }
    }
        
    @IBOutlet weak var graffitiTagrLabel: UILabel! {
        didSet {
            graffitiTagrLabel.text = Strings.appName
            graffitiTagrLabel.textColor = Colors.white
            graffitiTagrLabel.font = UIFont(name: Fonts.avenirLight, size: 36)
        }
    }
    
    @IBOutlet weak var sloganLabel: UILabel! {
        didSet {
            sloganLabel.text = Strings.slogan
            sloganLabel.textColor = Colors.white
            sloganLabel.font = UIFont(name: Fonts.avenirLight, size: 20)
        }
    }
    
    @IBOutlet weak var backgroundImageView: UIImageView! {
        didSet {
            backgroundImageView.image = UIImage(named: AssetNames.backgroundStartVC)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @objc func signUpButtonDidTap() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let welcomeController = storyboard.instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController else {
            debugPrint("Fail")
            return
            
        }
        self.navigationController?.pushViewController(welcomeController, animated: true)
    }
    
    
    @objc func loginButtonDidTap() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let welcomeController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else {
            debugPrint("Fail")
            return
            
        }
        self.navigationController?.pushViewController(welcomeController, animated: true)
    }
    
    
}
