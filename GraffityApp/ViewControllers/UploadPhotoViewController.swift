//
//  UploadPhotoViewController.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/16/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit
import Material

class UploadPhotoViewController: UIViewController {
    
    @IBOutlet var mainBackgroundView: UIView! {
        didSet {
            mainBackgroundView.backgroundColor = UIColor(named: Colors.backgroundName)
        }
    }
    
    @IBOutlet weak var screenTitleLabel: UILabel! {
        didSet {
            screenTitleLabel.text = Strings.addNew
            screenTitleLabel.font = UIFont(name: Fonts.avenirMedium, size: 20)
            screenTitleLabel.textColor = UIColor(named: Colors.textName)
        }
    }
    
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            imageView.layer.masksToBounds = true
            imageView.layer.cornerRadius = 5
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!  {
           didSet {
               titleLabel.text = Strings.title
               titleLabel.font = UIFont(name: Fonts.avenirMedium, size: 18)
            titleLabel.textColor = UIColor(named: Colors.textName)

           }
       }
    
    @IBOutlet weak var titleField: CustomErrorTextField! {
        didSet {
            titleField.placeholder = Strings.typeHere
            titleField.font = UIFont(name: Fonts.avenirLight, size: 20)
        }
    }
    
    @IBOutlet weak var backButton: UIButton! {
        didSet {
            backButton.setTitle(Strings.back.uppercased(), for: .normal)
            backButton.titleLabel?.font = UIFont(name: Fonts.avenirRoman, size: 16)
            backButton.backgroundColor = UIColor(named: Colors.backButtonName)
            backButton.setTitleColor(UIColor(named: Colors.textName), for: .normal)
            backButton.addTarget(self, action: #selector(backButtonDidTap), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var saveButton: UIButton! {
        didSet {
            saveButton.setTitle(Strings.save.uppercased(), for: .normal)
            saveButton.titleLabel?.font = UIFont(name: Fonts.avenirRoman, size: 16)
            saveButton.backgroundColor = UIColor(named: Colors.blueColorName)
            saveButton.setTitleColor(Colors.white, for: .normal)
            saveButton.addTarget(self, action: #selector(saveButtonDidTap), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var activitiIndicator: UIActivityIndicatorView!
    
    var image: UIImage?
    var metadata : [String : Any]?
    var source = "LIVE"
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = image
         
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        imageView.image = nil
//        image = nil
//        metadata = nil
//        source = "LIVE"
    }
    
    @objc func saveButtonDidTap() {
        if let image = image {
            
            let title = Settings.defaultGraffitiTitle
            
            var locInstance: Location?
            if let location = LocationManager.shared.locationManager.location?.coordinate {
                locInstance = Location(coordinates: location)
            }
            let imageToServer = ImageToServer(title: title, location: locInstance, source: source)
            activitiIndicator.startAnimating()
            RestAPI().createNewGraffity(data: imageToServer, image: image, metadata: metadata) { (isOk) in
                DispatchQueue.main.async {
                    debugPrint("success", isOk)
                    self.activitiIndicator.stopAnimating()
                    
                    
                    if !isOk {
                        self.showAlertOk(Strings.error, andText: Strings.somethingWentWrong)
                    } else {
                        self.goNext()
                    }
                }
            }
            
//            let options = [kCGImageSourceShouldCache: false]
//            if let data = image.pngData(){
//                let nsData = NSData(data: data)
//                guard let imgSrc = CGImageSourceCreateWithData(nsData, options as CFDictionary), let metadata = CGImageSourceCopyPropertiesAtIndex(imgSrc, 1, options as CFDictionary) as? NSDictionary else { return }
//
//                let gpsData = metadata[kCGImagePropertyGPSDictionary] as? [String : AnyObject]
//                debugPrint(gpsData, "gps data")
//            }
        }
    }
    
    @objc func goNext() {
        DispatchQueue.main.async {
            let tabBar = MainTabBarViewController()
            self.navigationController?.pushViewController(tabBar, animated: true)
          
        }
        
    }
    
    func checkTitle() -> Bool {
        if let empty = titleField.text?.isEmpty, !empty {
            return true
        }
        titleField.isErrorRevealed = true
        titleField.makeError(true)
        return false
    }
    
    @objc func backButtonDidTap() {
        let vcs = self.navigationController?.viewControllers
        if ((vcs?.contains(self)) != nil) {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }


}
