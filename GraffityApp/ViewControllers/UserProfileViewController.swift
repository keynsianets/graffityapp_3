//
//  UserProfileViewController.swift
//  GraffityApp
//
//  Created by Alex Suvorov on 9/4/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit

fileprivate let cellReuseIdentifier = "UserProfileSettingsTableViewCell"

class UserProfileViewController: UIViewController {

    
    @IBOutlet var mainBackgroundView: UIView! {
        didSet {
            mainBackgroundView.backgroundColor = UIColor(named: Colors.backgroundName)
        }
    }
    
    @IBOutlet weak var toastView: ToastView! {
        didSet {
            toastView.backgroundColor = Colors.scarlet
            toastView.isHidden = true
        }
    }
    @IBOutlet weak var tableView: UITableView!
    
    var settings = [UserSettingsCellData(image: #imageLiteral(resourceName: "edit"), title: Strings.editUserInformation), UserSettingsCellData(image: #imageLiteral(resourceName: "shape"), title: Strings.notifications), UserSettingsCellData(image: #imageLiteral(resourceName: "language"), title: Strings.languageSettings), UserSettingsCellData(image: #imageLiteral(resourceName: "logout"), title: Strings.logout)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: cellReuseIdentifier, bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }

}

extension UserProfileViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! UserProfileSettingsTableViewCell
        if indexPath.row < settings.count {
            cell.setData(data: settings[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        switch indexPath.row {
        case 0:
            let editProfileViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
            self.navigationController?.pushViewController(editProfileViewController, animated: true)
            return
        case 1:
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    UIApplication.shared.openURL(settingsUrl as URL)
                }
            }
            return
        case 2:
            toastView.showToast(text: "Coming soon", duration: 2)
            return
            
        case 3:
            // Create the alert controller
            let alertController = UIAlertController(title: "Log out?", message: "Are you shure you want to log out?", preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.destructive) {
                UIAlertAction in
                UserProfile.shared.token = nil
                NotificationCenter.default.post(name: .dissmis_tab_bar, object: nil)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            return
        default:
            return
        }
    }
    
    
}


struct UserSettingsCellData {
    var image: UIImage
    var title: String
    
    init(image: UIImage, title: String) {
        self.image = image
        self.title = title
    }
}
