//
//  ViewController.swift
//  GraffityApp
//
//  Created by Денис Марков on 4/10/19.
//  Copyright © 2019 KinectPro. All rights reserved.
//

import UIKit
import SCPageControl

fileprivate let slidesCount = 3

class WelcomeViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet var mainBackgroundView: UIView! {
        didSet {
            mainBackgroundView.backgroundColor = UIColor(named: Colors.backgroundName)
        }
    }
    
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var bottomView: UIView! {
        didSet {
            bottomView.layer.shadowOffset = CGSize(width: 0, height: -1)
            bottomView.layer.shadowOpacity = 0.15
            bottomView.layer.shadowRadius = 15.0
            bottomView.layer.shadowColor = UIColor.black.cgColor
            bottomView.backgroundColor = UIColor(named: Colors.tabBarName)
        }
    }
    @IBOutlet weak var pageContainerView: UIView!
    
    @IBOutlet weak var prevButton: UIButton! {
        didSet {
            prevButton.setTitle(Strings.prev.uppercased(), for: .normal)
            prevButton.titleLabel?.font = UIFont(name: Fonts.avenirBook, size: 14)
            prevButton.setTitleColor(UIColor(named: Colors.tabBarTextName), for: .normal)
            prevButton.addTarget(self, action: #selector(moveToPreviousPage), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var nextButton: UIButton! {
        didSet {
            nextButton.setTitle(Strings.next.uppercased(), for: .normal)
            nextButton.titleLabel?.font = UIFont(name: Fonts.avenirBook, size: 14)
            nextButton.setTitleColor(UIColor(named: Colors.tabBarTextName), for: .normal)
            nextButton.addTarget(self, action: #selector(moveToNextPage), for: .touchUpInside)
        }
    }
    
    let sc = SCPageControlView()
    
    var scrollView = UIScrollView()
    var pageControl = UIPageControl()
    var currentPage: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
          
        prevButton.isHidden = true
        
        UserProfile.shared.notNeedToShowWelcomeScreens = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setScrollView()
        setPagingView()
    }
    
    func setPagingView() {
        sc.frame = CGRect(x: 0, y: 0, width: pageContainerView.frame.width, height: pageContainerView.frame.height)
        sc.scp_style = .SCJAFillCircle
        let dotColor = UIColor(named: Colors.blueColorName) ?? Colors.darkSkyBlue
        sc.set_view(3, current: 0, current_color: dotColor, disable_color: .white)
        pageContainerView.addSubview(sc)
    }
    
    func setScrollView() {
        let width = contentView.frame.width
        let height = contentView.frame.height
        let scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        scrollView.isUserInteractionEnabled = true
        scrollView.isPagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        let firstContentView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        let firstScreen = StartScreenContentViewController(nibName: "StartScreenContentViewController", bundle: nil)
        firstContentView.addSubview(firstScreen.view)
        scrollView.addSubview(firstContentView)
        firstScreen.setContent(image: #imageLiteral(resourceName: "illustration"), title: Strings.protectYourCity, description: Strings.helpMakeBeautiful, size: contentView.frame.size)
        let secondContentView = UIView(frame: CGRect(x: width, y: 0, width: width, height: height))
        let secondScreen = StartScreenContentViewController(nibName: "StartScreenContentViewController", bundle: nil)
        secondContentView.addSubview(secondScreen.view)
        scrollView.addSubview(secondContentView)
        secondScreen.setContent(image: #imageLiteral(resourceName: "illustration-2"), title: Strings.shareYourSnap, description: Strings.takeAPhotoForPlaceToBeautiful, size: contentView.frame.size)
        let thirdContentView = UIView(frame: CGRect(x: width * 2, y: 0, width: width, height: height))
        let thirdScreen = StartScreenContentViewController(nibName: "StartScreenContentViewController", bundle: nil)
        thirdContentView.addSubview(thirdScreen.view)
        scrollView.addSubview(thirdContentView)
        thirdScreen.setContent(image: #imageLiteral(resourceName: "illustration-1"), title: Strings.cleanYourCity, description: Strings.helpToLiveInClean, size: contentView.frame.size)
        scrollView.contentSize = CGSize(width: width * CGFloat(slidesCount), height: height)
        scrollView.delegate = self
        self.scrollView = scrollView
        contentView.addSubview(scrollView)
    }
    
    //MARK: ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        sc.scroll_did(scrollView)
        prevButton.isHidden = scrollView.contentOffset.x == 0
    }
    
    @objc func moveToNextPage(){
        
        let pageWidth:CGFloat = self.scrollView.frame.width
        let maxWidth:CGFloat = pageWidth * CGFloat(slidesCount)
        let contentOffset:CGFloat = self.scrollView.contentOffset.x
        
        let slideToX = contentOffset + pageWidth
        
        if  contentOffset + pageWidth == maxWidth{
            goNext()
            return
        }
        self.scrollView.scrollRectToVisible(CGRect(x: slideToX, y: 0, width: pageWidth, height: self.scrollView.frame.height), animated: true)
        
        
    }
    
    func goNext() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let locationVC = storyboard.instantiateViewController(withIdentifier: "LocationRequestViewController") as! LocationRequestViewController
        self.navigationController?.pushViewController(locationVC, animated: true)
    }
    
    @objc func moveToPreviousPage(){
        
        let pageWidth:CGFloat = self.scrollView.frame.width
        let contentOffset:CGFloat = self.scrollView.contentOffset.x
        
        var slideToX = contentOffset - pageWidth
        
        if  contentOffset + pageWidth < 0 {
            slideToX = 0
        }
        self.scrollView.scrollRectToVisible(CGRect(x: slideToX, y: 0, width: pageWidth, height: self.scrollView.frame.height), animated: true)
  
    }

}




